from sklearn.cluster import AgglomerativeClustering, ward_tree
import numpy as np
import itertools
import matplotlib
matplotlib.use('Agg') 
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from scipy.stats import ks_2samp
from mpl_toolkits.mplot3d import Axes3D
from scipy.cluster.hierarchy import linkage, fcluster, dendrogram
import os
import yt
import scipy.spatial as spat
from scipy.spatial import KDTree


G=0.00430145 #gravitational constant in astronomical units [pc]*[MSun]**(-1)*[km/s]**2 (the same as Nbody6++GPU)

#Main
# It read sinks at a time t

t=3.0

gandnum=t*10.+1.
gandlab='{:02.0f}'.format(gandnum)

gasnum=t*100.
gaslab='{:03.0f}'.format(gasnum)

#GANDALF READING

x_gand=[]
y_gand=[]
z_gand=[]
vx_gand=[]
vy_gand=[]
vz_gand=[]
m_gand=[]

#GASOLINE READING, THROUGH YT

path = '/tank0/ballone/hydro_set_mcs_orig'

files = []
files2 = []
# r=root, d=directories, f = files
for r, d, f in os.walk(path):
    for file in f:
        if 'out.00300' in file and '.den' not in file: 
                files.append(os.path.join(r, file))
                 #print(files[-1])

numbers = np.array([1,10,11,2,3,4,5,6,7,8,9])
files.sort()
files_array = np.column_stack((numbers, files))
files_array = sorted(files_array, key = lambda x: float(x[0])) 
#print(files_array)
for i in range(len(files)):
    files[i] = files_array[i][1]
#files = np.sort(files)
print(files)

conv=2.22249e2 
labelsv = []  
nodes_ratios_vector = [] 
nodes_vector_vector = []
nodes_vector1_vector = []
nodes_vector2_vector = []
nodes_vector3_vector = []
nodes_vector4_vector = []
nodes_vector5_vector = []

dxcm_vector = []
dycm_vector = []
dzcm_vector = []
dcm_vector = []

dvxcm_vector = []
dvycm_vector = []
dvzcm_vector = []
dvcm_vector = []
theta_vector = []
dm_vector = []
dm_abs_vector = []
node_mass_vector = []

masses_vect = []
x_pos_vect = []
y_pos_vect = []
z_pos_vect = []

scales_m = []
scales_r = []
scales_v = []



#####################################################################################################################################

#ORIGINAL TREE

#####################################################################################################################################


fig10 = plt.figure(10, (10,10))
fig11 = plt.figure(11, (14,10))
fig12 = plt.figure(12, (14,10))
fig13 = plt.figure(13, (14,10))
fig14 = plt.figure(14, (14,10))
fig15 = plt.figure(15, (14,10))
fig16 = plt.figure(16, (14,10))


count=0


#First we create the "original" tree from which we will generate the new one
ind_sim = 0
Ni = 1
#files=files[:-2] #I descide to consider only the first simulation
print("\n")
print(files)

Nsim = 3 #Numero di simulazione = m(Nsim+1)e4
n_changes = 3 #Numero di split scambiati = n_changes-1

f_counter=0
colorv = ['orange','blue','green',"yellow","black",'red',"cyan","magenta","brown","pink"]
for f in files:
    #GANDALF READING

    print(f)

    x_gand=[]
    y_gand=[]
    z_gand=[]
    vx_gand=[]
    vy_gand=[]
    vz_gand=[]
    m_gand=[]

    #GASOLINE READING, THROUGH YT

    simname = f
    simnamered = str(f[34:39])
    labelsv.append(simnamered)
    print(simnamered)

    print("\n")

    ds=yt.load(f)
    dd=ds.all_data()

    #convert the mass from n-body units to solar
    m_gand=dd[('Stars',"Mass")]*conv
    x_gand=dd[('Stars',"particle_position_x")]
    y_gand=dd[('Stars',"particle_position_y")]
    z_gand=dd[('Stars',"particle_position_z")]
    vx_gand=dd[('Stars',"particle_velocity_x")]
    vy_gand=dd[('Stars',"particle_velocity_y")]
    vz_gand=dd[('Stars',"particle_velocity_z")]

    x_cm_sink = np.sum(x_gand*m_gand) /np.sum(m_gand)
    y_cm_sink = np.sum(y_gand*m_gand) /np.sum(m_gand)
    z_cm_sink = np.sum(z_gand*m_gand) /np.sum(m_gand)
    vx_cm_sink = np.sum(vx_gand*m_gand) /np.sum(m_gand)
    vy_cm_sink = np.sum(vy_gand*m_gand) /np.sum(m_gand)
    vz_cm_sink = np.sum(vz_gand*m_gand) /np.sum(m_gand)

    print "Sink center of mass position: ", x_cm_sink, " [pc] ", y_cm_sink, " [pc] ", z_cm_sink, " [pc] "
    print "Sink center of mass velocity: ", vx_cm_sink, " [km/s] ", vy_cm_sink, " [km/s] ", vz_cm_sink, " [km/s] "
    x_gand = x_gand - x_cm_sink
    y_gand = y_gand - y_cm_sink
    z_gand = z_gand - z_cm_sink
    vx_gand = vx_gand - vx_cm_sink
    vy_gand = vy_gand - vy_cm_sink
    vz_gand = vz_gand - vz_cm_sink

    if(f_counter==Nsim):
        x_gand_Nsim = x_gand 
        y_gand_Nsim = y_gand 
        z_gand_Nsim = z_gand 
        vx_gand_Nsim = vx_gand
        vy_gand_Nsim = vy_gand
        vz_gand_Nsim = vz_gand
        m_gand_Nsim = m_gand

        density_gand = np.zeros(len(m_gand))
    
        pos= np.stack((x_gand,y_gand,z_gand),axis=-1)
        pos_tree=spat.cKDTree(pos)
        denr=0.
        
        for i in np.arange(len(m_gand)):
            indneigh=pos_tree.query(pos[i],k=500)[1]
            dist_tree = np.sqrt((x_gand[indneigh]-x_gand[i])**2.+(y_gand[indneigh]-y_gand[i])**2.+(z_gand[indneigh]-z_gand[i])**2.)
            
            den=len(m_gand[indneigh])/((max(dist_tree))**3.)
            density_gand[i] = den
 
    Eksinkv= []
    Egsinkv= []

    dist = []
    distv = []

    for j in range(len(m_gand)):
        Eksinkv.append(1./2.*m_gand[j]*(vx_gand[j]**2+vy_gand[j]**2+vz_gand[j]**2))
        mvectappo = np.delete(m_gand,j)
        xvectappo = np.delete(x_gand,j)
        yvectappo = np.delete(y_gand,j)
        zvectappo = np.delete(z_gand,j)
        vxvectappo = np.delete(vx_gand,j)
        vyvectappo = np.delete(vy_gand,j)
        vzvectappo = np.delete(vz_gand,j)
        
        Egsinkv.append(np.sum(-0.5*G*m_gand[j]*mvectappo /np.sqrt((x_gand[j]-xvectappo)**2+(y_gand[j]-yvectappo)**2+(z_gand[j]-zvectappo)**2)))
        dist.append(np.sqrt((x_gand[j]-xvectappo)**2+(y_gand[j]-yvectappo)**2+(z_gand[j]-zvectappo)**2))
        distv.append(np.sqrt((vx_gand[j]-vxvectappo)**2+(vy_gand[j]-vyvectappo)**2+(vz_gand[j]-vzvectappo)**2))
    
    Eksink = sum(Eksinkv)
    Egsink = abs(sum(Egsinkv))
    vratio_sink = 2.*Eksink/Egsink
    if(f_counter==Nsim):
        vratio_sink_Nsim = vratio_sink

    f_counter+=1

    virial_radius = G*np.sum(m_gand)**2 /(2.*Egsink)
    t_scale = np.sqrt(virial_radius*virial_radius*virial_radius/(G*np.sum(m_gand)))
    v_scale = virial_radius/np.sqrt(virial_radius*virial_radius*virial_radius/(G*np.sum(m_gand)))

    m=np.array(m_gand)
    d_var = np.sqrt((np.var(x_gand)+np.var(y_gand)+np.var(z_gand))/3.)
    dv_var = np.sqrt((np.var(vx_gand)+np.var(vy_gand)+np.var(vz_gand))/3.)
    
    scales_m.append(np.sum(m_gand))
    scales_r.append(d_var)
    scales_v.append(dv_var)

    X=np.column_stack((x_gand/d_var,y_gand/d_var,z_gand/d_var,vx_gand/dv_var,vy_gand/dv_var,vz_gand/dv_var))
    V=np.column_stack((vx_gand/dv_var,vy_gand/dv_var,vz_gand/dv_var))
    i_sort=np.argsort(m_gand)
    print(np.sum(m_gand))

    #X=np.column_stack((x_gand/np.mean(dist),y_gand/np.mean(dist),z_gand/np.mean(dist)))
    #V=np.column_stack((vx_gand/np.mean(dist),vy_gand/np.mean(dist),vz_gand/np.mean(dist)))
    

    clustering = AgglomerativeClustering(n_clusters=X.shape[0],linkage="ward").fit(X) #n_clusters=X.shape[0] to make the full tree
    leaves_mass = np.zeros(X.shape[0]-1)

    #X=np.column_stack((x_gand,y_gand,z_gand))
    #V=np.column_stack((vx_gand,vy_gand,vz_gand))
    

    for i in range(X.shape[0]-1):
        for j in range(2):
            if(clustering.children_[i][j]<X.shape[0]):
                leaves_mass[i]+=m[clustering.children_[i][j]]
            else:
                leaves_mass[i]+=leaves_mass[clustering.children_[i][j]-X.shape[0]]


    nodes_ratios = np.zeros([X.shape[0]-1,2])

    nodes_ratios_vector.append(nodes_ratios[:,1])
    mcm = np.zeros([X.shape[0]-1,2])
    node_mass = np.zeros([X.shape[0]-1,1])
    masses = np.zeros([X.shape[0]-1,2])

    for i in range(X.shape[0]-1):
        for j in range(2):
            if(clustering.children_[i][j]<X.shape[0]):
                nodes_ratios[i][j]+=m[clustering.children_[i][j]]/leaves_mass[i]
                mcm[i][j]+=m[clustering.children_[i][j]]
                node_mass[i]+=m[clustering.children_[i][j]]
                masses[i][j]+=m[clustering.children_[i][j]]
                #nodes_ratios1[i][j]+=m[clustering.children_[i][j]]/(leaves_mass[i]-m[clustering.children_[i][j]])
            else:
                nodes_ratios[i][j]+=leaves_mass[clustering.children_[i][j]-X.shape[0]]/leaves_mass[i]
                mcm[i][j]+=leaves_mass[clustering.children_[i][j]-X.shape[0]]
                node_mass[i]+=leaves_mass[clustering.children_[i][j]-X.shape[0]]
                masses[i][j]+=leaves_mass[clustering.children_[i][j]-X.shape[0]]
                #nodes_ratios1[i][j]+=leaves_mass[clustering.children_[i][j]-X.shape[0]]/(leaves_mass[i]-leaves_mass[clustering.children_[i][j]-X.shape[0]])
        #print(nodes_ratios[i])

    dm = []
    dm_abs = []
    

    for i in range(len(nodes_ratios)):
        dm.append(nodes_ratios[i][0]-nodes_ratios[i][1]) 
        dm_abs.append(abs(nodes_ratios[i][0]-nodes_ratios[i][1])) 
        
    dm_vector.append(dm)
    dm_abs_vector.append(dm_abs)

    nodes_vector=np.array(nodes_ratios.flatten())

    nodes_vector_vector.append(nodes_vector)
    node_mass_vector.append(node_mass)
    masses_vect.append(masses)
    

    nodes_vector1 = nodes_vector[-10:]
    nodes_vector2 = nodes_vector[-100:]
    nodes_vector3 = nodes_vector[-10:]
    nodes_vector4 = nodes_vector[-2000:]
    nodes_vector5 = nodes_vector[:2000]

    nodes_vector1_vector.append(nodes_vector1)
    nodes_vector2_vector.append(nodes_vector2)
    nodes_vector3_vector.append(nodes_vector3)
    nodes_vector4_vector.append(nodes_vector4)
    nodes_vector5_vector.append(nodes_vector5)

    xcm = np.zeros([X.shape[0]-1,2])
    ycm = np.zeros([X.shape[0]-1,2])
    zcm = np.zeros([X.shape[0]-1,2])

    nodes_pos = np.zeros([X.shape[0]-1,1])

    for i in range(X.shape[0]-1):
        for j in range(2):
            if(clustering.children_[i][j]<X.shape[0]):
                xcm[i][j]+=X[clustering.children_[i][j]][0]
                ycm[i][j]+=X[clustering.children_[i][j]][1]
                zcm[i][j]+=X[clustering.children_[i][j]][2]
                #nodes_ratios1[i][j]+=m[clustering.children_[i][j]]/(leaves_mass[i]-m[clustering.children_[i][j]])
            else:	
                xcm[i][j]+=np.sum(mcm[clustering.children_[i][j]-X.shape[0]]*xcm[clustering.children_[i][j]-X.shape[0]])/np.sum(mcm[clustering.children_[i][j]-X.shape[0]])
                ycm[i][j]+=np.sum(mcm[clustering.children_[i][j]-X.shape[0]]*ycm[clustering.children_[i][j]-X.shape[0]])/np.sum(mcm[clustering.children_[i][j]-X.shape[0]])
                zcm[i][j]+=np.sum(mcm[clustering.children_[i][j]-X.shape[0]]*zcm[clustering.children_[i][j]-X.shape[0]])/np.sum(mcm[clustering.children_[i][j]-X.shape[0]])

    dcm = np.zeros(X.shape[0]-1)
    dxcm = np.zeros(X.shape[0]-1)
    dycm = np.zeros(X.shape[0]-1)
    dzcm = np.zeros(X.shape[0]-1)	
    dxcm1 = np.zeros(X.shape[0]-1)
    dycm1 = np.zeros(X.shape[0]-1)
    dzcm1 = np.zeros(X.shape[0]-1)	


    for i in range(X.shape[0]-1):
        dxcm[i]+=xcm[i][0]-xcm[i][1]
        dycm[i]+=ycm[i][0]-ycm[i][1]
        dzcm[i]+=zcm[i][0]-zcm[i][1]

        dcm[i]+=np.sqrt(dxcm[i]**2.+dycm[i]**2.+dzcm[i]**2.)

    for i in range(X.shape[0]-1):
        dxcm1[i]+=xcm[i][0]-xcm[i][1]
        dycm1[i]+=ycm[i][0]-ycm[i][1]
        dzcm1[i]+=zcm[i][0]-zcm[i][1]


    #dcm = dcm/dcm[X.shape[0]-2]
    #print(dcm)
    
    dxcm_vector.append(dxcm)
    dycm_vector.append(dycm)
    dzcm_vector.append(dzcm)
    dcm_vector.append(dcm)

    x_pos_vect.append(xcm)
    y_pos_vect.append(ycm)
    z_pos_vect.append(zcm)

    vxcm = np.zeros([V.shape[0]-1,2])
    vycm = np.zeros([V.shape[0]-1,2])
    vzcm = np.zeros([V.shape[0]-1,2])
    for i in range(V.shape[0]-1):
        for j in range(2):
            if(clustering.children_[i][j]<V.shape[0]):
                vxcm[i][j]+=V[clustering.children_[i][j]][0]
                vycm[i][j]+=V[clustering.children_[i][j]][1]
                vzcm[i][j]+=V[clustering.children_[i][j]][2]
                #nodes_ratios1[i][j]+=m[clustering.children_[i][j]]/(leaves_mass[i]-m[clustering.children_[i][j]])
            else:	
                vxcm[i][j]+=np.sum(mcm[clustering.children_[i][j]-V.shape[0]]*vxcm[clustering.children_[i][j]-V.shape[0]])/np.sum(mcm[clustering.children_[i][j]-V.shape[0]])
                vycm[i][j]+=np.sum(mcm[clustering.children_[i][j]-V.shape[0]]*vycm[clustering.children_[i][j]-V.shape[0]])/np.sum(mcm[clustering.children_[i][j]-V.shape[0]])
                vzcm[i][j]+=np.sum(mcm[clustering.children_[i][j]-V.shape[0]]*vzcm[clustering.children_[i][j]-V.shape[0]])/np.sum(mcm[clustering.children_[i][j]-V.shape[0]])

    dvcm = np.zeros(V.shape[0]-1)
    dvxcm = np.zeros(V.shape[0]-1)
    dvycm = np.zeros(V.shape[0]-1)
    dvzcm = np.zeros(V.shape[0]-1)

    dvxcm1 = np.zeros(V.shape[0]-1)
    dvycm1 = np.zeros(V.shape[0]-1)
    dvzcm1 = np.zeros(V.shape[0]-1)	
    

    for i in range(V.shape[0]-1):
        dvxcm[i]+=vxcm[i][0]-vxcm[i][1]
        dvycm[i]+=vycm[i][0]-vycm[i][1]
        dvzcm[i]+=vzcm[i][0]-vzcm[i][1]

        dvcm[i]+=np.sqrt(dvxcm[i]**2.+dvycm[i]**2.+dvzcm[i]**2.)

    for i in range(V.shape[0]-1):
        dvxcm1[i]+=vxcm[i][0]-vxcm[i][1]
        dvycm1[i]+=vycm[i][0]-vycm[i][1]
        dvzcm1[i]+=vzcm[i][0]-vzcm[i][1]

    #dvcm = dvcm/dvcm[X.shape[0]-2]
    dvxcm_vector.append(dvxcm)
    dvycm_vector.append(dvycm)
    dvzcm_vector.append(dvzcm)
    dvcm_vector.append(dvcm)

    theta = np.zeros(V.shape[0]-1)
    
    for i in range(len(dvcm)):
        theta[i] = np.arccos((dxcm1[i]*dvxcm1[i] + dycm1[i]*dvycm1[i] + dzcm1[i]*dvzcm1[i]) / (dcm[i]*dvcm[i]))  
        
    theta_vector.append(theta)

    Ni+=1


appo = np.linspace(1,len(dm_vector[0]),len(dm_vector[0]))
appo = appo[::-1]

fig24 = plt.figure(24, (14,10))
plt.scatter(dm_vector[0], node_mass_vector[0], c=appo, norm=colors.LogNorm())
plt.xlabel(r'$dm$',fontsize=16) 
plt.ylabel(r'$m_{node}$',fontsize=16) 
plt.xscale('log')
plt.yscale('log')
plt.colorbar().ax.set_ylabel(r'$N_{split}$', rotation=270, fontsize=16,labelpad=15)
plt.savefig("./relation_"+str(Nsim)+".png")
plt.close()


masses_vect[Nsim] = masses_vect[Nsim][::-1]
x_pos_vect[Nsim] = x_pos_vect[Nsim][::-1]
y_pos_vect[Nsim] = y_pos_vect[Nsim][::-1]
z_pos_vect[Nsim] = z_pos_vect[Nsim][::-1]

for i in range(3):
    seed = np.random.randint(1000)
    np.random.seed(seed)

    #Plot the original distribution

    fig3= plt.figure(3,figsize=(12,8))
    fig9= plt.figure(9,figsize=(16,8))
    fig40= plt.figure(40,figsize=(16,8))

    plt.figure(3)
    weights= np.zeros(len(dcm_vector[Nsim]))+1./float(len(dcm_vector[Nsim]))

    fig3.add_subplot(221)
    plt.hist(dm_abs_vector[Nsim], bins=np.logspace(np.log10(min(dm_abs_vector[Nsim])),np.log10(max(dm_abs_vector[Nsim])),30), weights = weights,  histtype='stepfilled', facecolor='None', edgecolor="Crimson", linewidth=2.)

    fig3.add_subplot(222)
    plt.hist(abs(np.array(dxcm_vector[Nsim])), bins=np.logspace(np.log10(min(abs(np.array(dxcm_vector[Nsim])))),np.log10(max(abs(np.array(dxcm_vector[Nsim])))),30), weights = weights,  histtype='stepfilled', facecolor='None', edgecolor="Crimson", linewidth=2.)

    fig3.add_subplot(223)
    plt.hist(abs(np.array(dycm_vector[Nsim])), bins=np.logspace(np.log10(min(abs(np.array(dycm_vector[Nsim])))),np.log10(max(abs(np.array(dycm_vector[Nsim])))),30), weights = weights,  histtype='stepfilled', facecolor='None', edgecolor="Crimson", linewidth=2.)

    fig3.add_subplot(224)
    plt.hist(abs(np.array(dzcm_vector[Nsim])), bins=np.logspace(np.log10(min(abs(np.array(dzcm_vector[Nsim])))),np.log10(max(abs(np.array(dzcm_vector[Nsim])))),30), weights = weights,  histtype='stepfilled', facecolor='None', edgecolor="Crimson", linewidth=2.)


    appo = np.linspace(1,len(dm_vector[Nsim]),len(dm_vector[Nsim]))
    appo = appo[::-1]

    plt.figure(9)
    fig9.add_subplot(1,4,1)
    plt.plot(appo, dm_vector[Nsim], lw=0.00001, alpha=0.5, marker=".", color="Orange")

    fig9.add_subplot(1,4,2)
    plt.plot(appo, dcm_vector[Nsim], lw=0.00001, alpha=0.5, marker=".", color="Orange")

    fig9.add_subplot(1,4,3)
    plt.plot(appo, dvcm_vector[Nsim], lw=0.00001, alpha=0.5, marker=".", color="Orange")

    fig9.add_subplot(1,4,4)
    plt.plot(appo, theta_vector[Nsim], lw=0.00001, alpha=0.5, marker=".", color="Orange")

    plt.tight_layout()     

    appo = np.linspace(1,len(dm_vector[Nsim]),len(dm_vector[Nsim]))
    appo = appo[::-1]
    old_node_mass_vector = node_mass_vector[Nsim]

    plt.figure(40)
    plt.plot(appo, old_node_mass_vector, lw=0.00001, alpha=0.5, marker=".", color="Orange", label="Original")

    #####################################################################################################################################

    #NEW TREE

    #####################################################################################################################################

    #Now we generate the new tree. 

    #Create the new vector of mass ratios, c.o.m. distances and velocity c.o.m. distances
    new_mass_vector = np.zeros(len(dcm_vector[Nsim]))
    print("Len", len(new_mass_vector))
    new_mass_vector_theo = np.zeros(len(dcm_vector[Nsim]))
    
    new_dcm_vector = np.zeros(len(dcm_vector[Nsim]))
    new_dvcm_vector = np.zeros(len(dcm_vector[Nsim]))

    new_dxcm_vector = np.zeros(len(dxcm_vector[Nsim]))
    new_dycm_vector = np.zeros(len(dycm_vector[Nsim]))
    new_dzcm_vector = np.zeros(len(dzcm_vector[Nsim]))

    new_dvxcm_vector = np.zeros(len(dvxcm_vector[Nsim]))
    new_dvycm_vector = np.zeros(len(dvycm_vector[Nsim]))
    new_dvzcm_vector = np.zeros(len(dvzcm_vector[Nsim]))

    new_theta_vector = np.zeros(len(dcm_vector[Nsim]))

    node_mass_vector_inv = node_mass_vector[Nsim][::-1]
    new_node_mass_vector = np.zeros(len(node_mass_vector[Nsim]))

    appo_sign=1.

    print(len(dcm_vector[Nsim]))
    new_index= len(dcm_vector[Nsim])-1
    
    while(new_index>=0):
        i_range = new_index

        rand_lim = len(dcm_vector[Nsim]) - n_changes            

        new_mass_vector_theo[new_index] = dm_vector[Nsim][i_range]
        new_dcm_vector[new_index] = dcm_vector[Nsim][i_range]#+appo_sign*dcm_vector[Nsim][i_range]
        new_dvcm_vector[new_index] = dvcm_vector[Nsim][i_range]#+appo_sign*dvcm_vector[Nsim][i_range]
        new_theta_vector[new_index] = theta_vector[Nsim][i_range]#+appo_sign*theta_vector[Nsim][i_range]
           
        if(new_index>rand_lim):
            Nsim_rand = Nsim
            sigma_rand = 0.0

            while(Nsim_rand==Nsim):
                Nsim_rand = np.random.randint(len(dcm_vector))
            print("Nsim rand", Nsim_rand)
            
            index_rand = i_range-len(dxcm_vector[Nsim]) #int(float(i_range)*float(len(dcm_vector[Nsim]))/float(len(dcm_vector[Nsim_rand]))) 
                        
            appo_rand_x = dxcm_vector[Nsim_rand][index_rand] #np.random.choice([dxcm_vector[Nsim_rand][index_rand],dycm_vector[Nsim_rand][index_rand],dzcm_vector[Nsim_rand][index_rand]])
            appo_rand_y = dycm_vector[Nsim_rand][index_rand] #np.random.choice([dxcm_vector[Nsim_rand][index_rand],dycm_vector[Nsim_rand][index_rand],dzcm_vector[Nsim_rand][index_rand]])
            appo_rand_z = dzcm_vector[Nsim_rand][index_rand] #np.random.choice([dxcm_vector[Nsim_rand][index_rand],dycm_vector[Nsim_rand][index_rand],dzcm_vector[Nsim_rand][index_rand]])

            appo_rand_vx = dvxcm_vector[Nsim_rand][index_rand] #np.random.choice([dvxcm_vector[Nsim_rand][index_rand],dvycm_vector[Nsim_rand][index_rand],dvzcm_vector[Nsim_rand][index_rand]])
            appo_rand_vy = dvycm_vector[Nsim_rand][index_rand] #np.random.choice([dvxcm_vector[Nsim_rand][index_rand],dvycm_vector[Nsim_rand][index_rand],dvzcm_vector[Nsim_rand][index_rand]])
            appo_rand_vz = dvzcm_vector[Nsim_rand][index_rand] #np.random.choice([dvxcm_vector[Nsim_rand][index_rand],dvycm_vector[Nsim_rand][index_rand],dvzcm_vector[Nsim_rand][index_rand]])   

            appo_sign = np.random.uniform(-0.00,0.00)
            if(dm_vector[Nsim][i_range]+appo_sign*dm_vector[Nsim][i_range]<0.98):
                new_mass_vector[new_index] = dm_vector[Nsim_rand][index_rand]+appo_sign*dm_vector[Nsim_rand][index_rand]
            else:
                new_mass_vector[new_index] = dm_vector[Nsim_rand][index_rand]

        #elif(new_index>rand_lim-1):
        #    Nsim_rand = Nsim
        #    index_rand = i_range
        #    sigma_rand = 0.0

        #    appo_rand_x = np.random.choice([dxcm_vector[Nsim][i_range],dycm_vector[Nsim][i_range],dzcm_vector[Nsim][i_range]])
        #    appo_rand_y = np.random.choice([dxcm_vector[Nsim][i_range],dycm_vector[Nsim][i_range],dzcm_vector[Nsim][i_range]])
        #    appo_rand_z = np.random.choice([dxcm_vector[Nsim][i_range],dycm_vector[Nsim][i_range],dzcm_vector[Nsim][i_range]])

        #    appo_rand_vx = np.random.choice([dvxcm_vector[Nsim][i_range],dvycm_vector[Nsim][i_range],dvzcm_vector[Nsim][i_range]])
        #    appo_rand_vy = np.random.choice([dvxcm_vector[Nsim][i_range],dvycm_vector[Nsim][i_range],dvzcm_vector[Nsim][i_range]])
        #    appo_rand_vz = np.random.choice([dvxcm_vector[Nsim][i_range],dvycm_vector[Nsim][i_range],dvzcm_vector[Nsim][i_range]])   

        else:
            sigma_rand = 0.0
            appo_sign = 0.0
            
            appo_rand_x = dxcm_vector[Nsim][i_range]+appo_sign*dxcm_vector[Nsim][i_range]
            appo_rand_y = dycm_vector[Nsim][i_range]+appo_sign*dycm_vector[Nsim][i_range]
            appo_rand_z = dzcm_vector[Nsim][i_range]+appo_sign*dzcm_vector[Nsim][i_range]

            appo_rand_vx = dvxcm_vector[Nsim][i_range]+appo_sign*dvxcm_vector[Nsim][i_range]
            appo_rand_vy = dvycm_vector[Nsim][i_range]+appo_sign*dvycm_vector[Nsim][i_range]
            appo_rand_vz = dvzcm_vector[Nsim][i_range]+appo_sign*dvzcm_vector[Nsim][i_range]

            new_mass_vector[new_index] = dm_vector[Nsim][i_range]
        
        appo_sign = np.random.normal(-sigma_rand,sigma_rand,6) #np.random.normal(0.,sigma_rand)#*(np.exp(i/1000)-1)
        
        new_dxcm_vector[new_index] =  appo_rand_x+appo_sign[0]*appo_rand_x                   
        new_dycm_vector[new_index] =  appo_rand_y+appo_sign[1]*appo_rand_y
        new_dzcm_vector[new_index] =  appo_rand_z+appo_sign[2]*appo_rand_z

        new_dvxcm_vector[new_index] =  appo_rand_vx+appo_sign[3]*appo_rand_vx
        new_dvycm_vector[new_index] =  appo_rand_vy+appo_sign[4]*appo_rand_vy
        new_dvzcm_vector[new_index] =  appo_rand_vz+appo_sign[5]*appo_rand_vz
        
        new_index+=-1

    #We now have created new vectors of distances in the mass, position of velocity space. In order to create a new stellar system we need to split the distances in separate stars


    #Create the new masses, positions and velocities from the tree just sampled

    new_splitted_theo_m = np.zeros((len(dcm_vector[Nsim])+1,2))

    new_splitted_m = np.zeros((len(dcm_vector[Nsim])+1,2))
    new_splitted_x = np.zeros((len(dcm_vector[Nsim])+1,2))
    new_splitted_y = np.zeros((len(dcm_vector[Nsim])+1,2))
    new_splitted_z = np.zeros((len(dcm_vector[Nsim])+1,2))

    new_splitted_vx = np.zeros((2*len(dcm_vector[Nsim])+1,2))
    new_splitted_vy = np.zeros((2*len(dcm_vector[Nsim])+1,2))
    new_splitted_vz = np.zeros((2*len(dcm_vector[Nsim])+1,2))



    new_splitted_labels = np.zeros((len(dcm_vector[Nsim])+1,2))


    appo = np.linspace(1,len(dm_vector[Nsim]),len(dm_vector[Nsim]))
    appo = appo[::-1]

    plt.figure(9)
    fig9.add_subplot(1,4,1)
    plt.plot(appo, new_mass_vector, lw=0.00001, alpha=0.5, marker=".", color=colorv[1])

    fig9.add_subplot(1,4,2)
    plt.plot(appo, new_dcm_vector, lw=0.00001, alpha=0.5, marker=".", color=colorv[1])
    plt.legend()

    fig9.add_subplot(1,4,3)
    plt.plot(appo, new_dvcm_vector, lw=0.00001, alpha=0.5, marker=".", color=colorv[1])

    fig9.add_subplot(1,4,4)
    plt.plot(appo, new_theta_vector, lw=0.00001, alpha=0.5, marker=".", color=colorv[1])


    plt.figure(9)
    fig9.add_subplot(1,4,1)
    plt.xlabel(r'$N_{splitting}$') 
    plt.ylabel(r'$\Delta m_{splitting}$') 
    plt.xscale('log')
    plt.yscale('log')
    #plt.ylim(0.0002,1.1)

    fig9.add_subplot(1,4,2)
    plt.xlabel(r'$N_{splitting}$') 
    plt.ylabel(r'$d_{cm}$') 
    plt.xscale('log')
    plt.yscale('log')


    fig9.add_subplot(1,4,3)
    plt.xlabel(r'$N_{splitting}$') 
    plt.ylabel(r'$dv_{cm}$') 
    plt.xscale('log')
    plt.yscale('log')

    fig9.add_subplot(1,4,4)
    plt.xlabel(r'$N_{splitting}$') 
    plt.ylabel(r'$\theta$') 
    plt.xscale('log')
    #plt.yscale('log')

    plt.tight_layout()     
    plt.savefig("./new_parameters"+str(seed)+"_"+str(Nsim)+".png") 


    weights= np.zeros(len(dcm_vector[Nsim]))+1./float(len(dcm_vector[Nsim]))
    
    plt.figure(3)
    fig3.add_subplot(221)
    plt.hist(abs(new_mass_vector), bins=np.logspace(np.log10(min(abs(new_mass_vector))),np.log10(max(abs(new_mass_vector))),30), weights = weights,  histtype='stepfilled', facecolor='None', edgecolor=colorv[1], linewidth=2.)
    #plt.hist(new_mass_vector, bins=np.logspace(np.log10(min(new_mass_vector)),np.log10(max(new_mass_vector)),30), weights = weights,  histtype='stepfilled', facecolor='None', edgecolor="Orange", linewidth=2.)
    plt.xlabel(r'$dm$')
    plt.xscale('log')

    fig3.add_subplot(222)
    plt.hist(abs(new_dxcm_vector), bins=np.logspace(np.log10(min(abs(new_dxcm_vector))),np.log10(max(abs(new_dxcm_vector))),30), weights = weights,  histtype='stepfilled', facecolor='None', edgecolor=colorv[1],  linewidth=2.)
    #plt.hist(new_dcm_vector, bins=np.logspace(np.log10(min(new_dcm_vector)),np.log10(max(new_dcm_vector)),30), weights = weights,  histtype='stepfilled', facecolor='None', edgecolor="Orange", linewidth=2.)
    plt.legend()
    plt.xlabel(r'$d_{cm}$')
    plt.xscale('log')

    fig3.add_subplot(223)
    plt.hist(abs(new_dycm_vector), bins=np.logspace(np.log10(min(abs(new_dycm_vector))),np.log10(max(abs(new_dycm_vector))),30), weights = weights,  histtype='stepfilled', facecolor='None', edgecolor=colorv[1],  linewidth=2.)
    #plt.hist(new_dvcm_vector, bins=np.logspace(np.log10(min(new_dvcm_vector)),np.log10(max(new_dvcm_vector)),30), weights = weights,  histtype='stepfilled', facecolor='None', edgecolor="Orange", linewidth=2.)
    plt.xlabel(r'$dv_{cm}$')
    plt.xscale('log')

    fig3.add_subplot(224)
    plt.hist(abs(new_dzcm_vector), bins=np.logspace(np.log10(min(abs(new_dzcm_vector))),np.log10(max(abs(new_dzcm_vector))),30), weights = weights,  histtype='stepfilled', facecolor='None', edgecolor=colorv[1], linewidth=2.)
    #plt.hist(new_theta_vector, bins=np.linspace(min(new_theta_vector),max(new_theta_vector),30), weights = weights,  histtype='stepfilled', facecolor='None', edgecolor="Orange", linewidth=2.)
    plt.xlabel(r'$\theta_{cm}$')
    plt.xscale('log')

    fig3.savefig('./new_distributions'+str(seed)+"_"+str(Nsim)+'.png')
    plt.close()


    #print(min(abs(new_dycm_vector)),min(abs(dycm_vector[Nsim])))

    #Reverse the vectors (the leaves now are at the end)

    new_mass_vector=new_mass_vector[::-1]
    new_mass_vector_theo=new_mass_vector_theo[::-1]
    new_dxcm_vector=new_dxcm_vector[::-1]
    new_dycm_vector=new_dycm_vector[::-1]
    new_dzcm_vector=new_dzcm_vector[::-1]
    new_dvxcm_vector=new_dvxcm_vector[::-1]
    new_dvycm_vector=new_dvycm_vector[::-1]
    new_dvzcm_vector=new_dvzcm_vector[::-1]

    #print(new_node_mass_vector[0])
    
    parent_vector= np.zeros((len(dcm_vector[Nsim])+1,2)) #In this vector we will put the branches that can be splitted again (it an appo matrix)
    parent_vector_label= np.zeros((len(dcm_vector[Nsim])+1,2)) #In this vector we will put the branches that can be splitted again (it an appo matrix)

    parent_vector_theo= np.zeros((len(dcm_vector[Nsim])+1,2)) #In this vector we will put the branches that can be splitted again (it an appo matrix)
    parent_vector_theo_label= np.zeros((len(dcm_vector[Nsim])+1,2)) #In this vector we will put the branches that can be splitted again (it an appo matrix)

    x_leaves = np.zeros((len(dcm_vector[Nsim])+1,2)) #In this vector we will put the branches that can be splitted again (it an appo matrix)
    y_leaves = np.zeros((len(dcm_vector[Nsim])+1,2)) #In this vector we will put the branches that can be splitted again (it an appo matrix)
    z_leaves = np.zeros((len(dcm_vector[Nsim])+1,2)) #In this vector we will put the branches that can be splitted again (it an appo matrix)
    vx_leaves = np.zeros((len(dcm_vector[Nsim])+1,2)) #In this vector we will put the branches that can be splitted again (it an appo matrix)
    vy_leaves = np.zeros((len(dcm_vector[Nsim])+1,2)) #In this vector we will put the branches that can be splitted again (it an appo matrix)
    vz_leaves = np.zeros((len(dcm_vector[Nsim])+1,2)) #In this vector we will put the branches that can be splitted again (it an appo matrix)

    x_leaves_label = np.zeros((len(dcm_vector[Nsim])+1,2)) #In this vector we will put the branches that can be splitted again (it an appo matrix)
    y_leaves_label = np.zeros((len(dcm_vector[Nsim])+1,2)) #In this vector we will put the branches that can be splitted again (it an appo matrix)
    z_leaves_label = np.zeros((len(dcm_vector[Nsim])+1,2)) #In this vector we will put the branches that can be splitted again (it an appo matrix)
    vx_leaves_label = np.zeros((len(dcm_vector[Nsim])+1,2)) #In this vector we will put the branches that can be splitted again (it an appo matrix)
    vy_leaves_label = np.zeros((len(dcm_vector[Nsim])+1,2)) #In this vector we will put the branches that can be splitted again (it an appo matrix)
    vz_leaves_label = np.zeros((len(dcm_vector[Nsim])+1,2)) #In this vector we will put the branches that can be splitted again (it an appo matrix)

    
    label_counter = 1
    i=0
    appo_leaves =0
    while(i<len(new_mass_vector)):
        if(i==0): #First splitting
            #take the i-th splitting and separate the masses
            

            new_splitted_m[i,0]=(new_mass_vector[i]+1.)/2.*scales_m[Nsim] 
            new_splitted_m[i,1]=(1.-new_mass_vector[i])/2.*scales_m[Nsim] 

            mass_of_branch = new_splitted_m[i,0] + new_splitted_m[i,1]

            new_splitted_theo_m[i,0]=(new_mass_vector_theo[i]+1.)/2.*scales_m[Nsim] #Multiply for the mass scale to return to physical units
            new_splitted_theo_m[i,1]=(1.-new_mass_vector_theo[i])/2.*scales_m[Nsim]

            new_node_mass_vector[i] = mass_of_branch

            parent_vector[i,0]=new_splitted_m[i,0] 
            parent_vector[i,1]=new_splitted_m[i,1]

            parent_vector_theo[i,0]=new_splitted_theo_m[i,0] 
            parent_vector_theo[i,1]=new_splitted_theo_m[i,1]


            new_splitted_labels[i,0] = label_counter
            label_counter+=1
            new_splitted_labels[i,1] = label_counter

            #Now split the positions and velocities
            
            new_splitted_x[i,0]= (0. + new_dxcm_vector[i]*(new_splitted_m[i,1])/mass_of_branch)#*scales_r[Nsim]
            new_splitted_x[i,1]= (0. - new_dxcm_vector[i]*(new_splitted_m[i,0])/mass_of_branch)#*scales_r[Nsim]

            new_splitted_y[i,0]= (0. + new_dycm_vector[i]*(new_splitted_m[i,1])/mass_of_branch)#*scales_r[Nsim]
            new_splitted_y[i,1]= (0. - new_dycm_vector[i]*(new_splitted_m[i,0])/mass_of_branch)#*scales_r[Nsim]

            new_splitted_z[i,0]= (0. + new_dzcm_vector[i]*(new_splitted_m[i,1])/mass_of_branch)#*scales_r[Nsim]
            new_splitted_z[i,1]= (0. - new_dzcm_vector[i]*(new_splitted_m[i,0])/mass_of_branch)#*scales_r[Nsim]

            new_splitted_vx[i,0]= (0. + new_dvxcm_vector[i]*(new_splitted_m[i,1])/mass_of_branch)#*scales_v[Nsim]
            new_splitted_vx[i,1]= (0. - new_dvxcm_vector[i]*(new_splitted_m[i,0])/mass_of_branch)#*scales_v[Nsim]

            new_splitted_vy[i,0]= (0. + new_dvycm_vector[i]*(new_splitted_m[i,1])/mass_of_branch)#*scales_v[Nsim]
            new_splitted_vy[i,1]= (0. - new_dvycm_vector[i]*(new_splitted_m[i,0])/mass_of_branch)#*scales_v[Nsim]

            new_splitted_vz[i,0]= (0. + new_dvzcm_vector[i]*(new_splitted_m[i,1])/mass_of_branch)#*scales_v[Nsim]
            new_splitted_vz[i,1]= (0. - new_dvzcm_vector[i]*(new_splitted_m[i,0])/mass_of_branch)#*scales_v[Nsim]

            #if(abs(new_splitted_x[i]-x_pos_vect[Nsim][i])[0]>0.001):
            #    print("First splitting: ", new_splitted_m[0],masses_vect[Nsim][0], new_splitted_x[0],x_pos_vect[Nsim][0])


            #Add the new splitting to the leaves vectors
            for k in range(2):
                parent_vector[i,k]=new_splitted_m[i,k]
                parent_vector_label[i,k]= 2

                parent_vector_theo[i,k]=new_splitted_theo_m[i,k]
                parent_vector_theo_label[i,k]= 2

                x_leaves[i,k] = new_splitted_x[i,k]
                y_leaves[i,k] = new_splitted_y[i,k]
                z_leaves[i,k] = new_splitted_z[i,k]
                vx_leaves[i,k] = new_splitted_vx[i,k]
                vy_leaves[i,k] = new_splitted_vy[i,k]
                vz_leaves[i,k] = new_splitted_vz[i,k]

                x_leaves_label[i,k] = 2 #new_splitted_x[ind0,ind1])
                y_leaves_label[i,k] = 2 #new_splitted_y[ind0,ind1])
                z_leaves_label[i,k] = 2 #new_splitted_z[ind0,ind1])
                vx_leaves_label[i,k] = 2 #new_splitted_x[ind0,ind1])
                vy_leaves_label[i,k] = 2 #new_splitted_y[ind0,ind1])
                vz_leaves_label[i,k] = 2 #new_splitted_z[ind0,ind1])
                
            if(i<20):
                print(i, new_splitted_m[i], scales_m[Nsim], new_splitted_x[i],x_pos_vect[Nsim][i])


        else:
            #We have to chose under which criterion to select the mass to split
            #We decide to take from the pick_up_vector three random masses and from these masses we select the most massive one     
            #print(i)
            
            m_to_split= node_mass_vector_inv[i][0] #max(num_to_split)
            choice_vector = np.where(parent_vector_theo_label[:,:]==2, parent_vector_theo,0.)
            ind_to_split= np.where(np.isclose(10000.*choice_vector,10000.*m_to_split)) #Find the indexes of the masses to split in the parent_vector

            #print(ind_to_split, parent_vector_theo)           
            
            ind0= ind_to_split[0][0]
            ind1= ind_to_split[1][0]

            mass_of_branch = new_splitted_m[ind0,ind1]
            new_node_mass_vector[i] = mass_of_branch

            new_splitted_m[i,1]=(1.-new_mass_vector[i])*mass_of_branch/2.
            new_splitted_m[i,0]=(new_mass_vector[i]+1.)*mass_of_branch/2.

            mass_of_branch_theo = new_splitted_theo_m[ind0,ind1]

            #print(mass_of_branch_theo)

            new_splitted_theo_m[i,1]=(1.-new_mass_vector_theo[i])*mass_of_branch_theo/2.
            new_splitted_theo_m[i,0]=(new_mass_vector_theo[i]+1.)*mass_of_branch_theo/2.
            
            #Separate the new positions and velocities from that of the splitted mass
            
            
            new_splitted_x[i,0]= (new_splitted_x[ind0,ind1] + new_dxcm_vector[i]*(new_splitted_m[i,1])/mass_of_branch)
            new_splitted_x[i,1]= (new_splitted_x[ind0,ind1] - new_dxcm_vector[i]*(new_splitted_m[i,0])/mass_of_branch)

            new_splitted_y[i,0]= (new_splitted_y[ind0,ind1] + new_dycm_vector[i]*(new_splitted_m[i,1])/mass_of_branch)
            new_splitted_y[i,1]= (new_splitted_y[ind0,ind1] - new_dycm_vector[i]*(new_splitted_m[i,0])/mass_of_branch)

            new_splitted_z[i,0]= (new_splitted_z[ind0,ind1] + new_dzcm_vector[i]*(new_splitted_m[i,1])/mass_of_branch)
            new_splitted_z[i,1]= (new_splitted_z[ind0,ind1] - new_dzcm_vector[i]*(new_splitted_m[i,0])/mass_of_branch)

            new_splitted_vx[i,0]= (new_splitted_vx[ind0,ind1] + new_dvxcm_vector[i]*(new_splitted_m[i,1])/mass_of_branch)
            new_splitted_vx[i,1]= (new_splitted_vx[ind0,ind1] - new_dvxcm_vector[i]*(new_splitted_m[i,0])/mass_of_branch)

            new_splitted_vy[i,0]= (new_splitted_vy[ind0,ind1] + new_dvycm_vector[i]*(new_splitted_m[i,1])/mass_of_branch)
            new_splitted_vy[i,1]= (new_splitted_vy[ind0,ind1] - new_dvycm_vector[i]*(new_splitted_m[i,0])/mass_of_branch)

            new_splitted_vz[i,0]= (new_splitted_vz[ind0,ind1] + new_dvzcm_vector[i]*(new_splitted_m[i,1])/mass_of_branch)
            new_splitted_vz[i,1]= (new_splitted_vz[ind0,ind1] - new_dvzcm_vector[i]*(new_splitted_m[i,0])/mass_of_branch)

            #Add the new splitting to the leaves vectors 
            for k in range(2):
                parent_vector[i,k]=new_splitted_m[i,k]
                parent_vector_label[i,k]= 2

                parent_vector_theo[i,k]=new_splitted_theo_m[i,k]
                parent_vector_theo_label[i,k]= 2

                x_leaves[i,k] = new_splitted_x[i,k]
                y_leaves[i,k] = new_splitted_y[i,k]
                z_leaves[i,k] = new_splitted_z[i,k]
                vx_leaves[i,k] = new_splitted_vx[i,k]
                vy_leaves[i,k] = new_splitted_vy[i,k]
                vz_leaves[i,k] = new_splitted_vz[i,k]

                x_leaves_label[i,k] = 2 #new_splitted_x[ind0,ind1])
                y_leaves_label[i,k] = 2 #new_splitted_y[ind0,ind1])
                z_leaves_label[i,k] = 2 #new_splitted_z[ind0,ind1])
                vx_leaves_label[i,k] = 2 #new_splitted_x[ind0,ind1])
                vy_leaves_label[i,k] = 2 #new_splitted_y[ind0,ind1])
                vz_leaves_label[i,k] = 2 #new_splitted_z[ind0,ind1])
                    
            #The splitted particle is no more a leave, they are a branch
            x_leaves_label[ind0,ind1] = 1 #new_splitted_x[ind0,ind1])
            y_leaves_label[ind0,ind1] = 1 #new_splitted_y[ind0,ind1])
            z_leaves_label[ind0,ind1] = 1 #new_splitted_z[ind0,ind1])
            vx_leaves_label[ind0,ind1] = 1 #new_splitted_x[ind0,ind1])
            vy_leaves_label[ind0,ind1] = 1 #new_splitted_y[ind0,ind1])
            vz_leaves_label[ind0,ind1] = 1 #new_splitted_z[ind0,ind1])

            parent_vector_label[ind0,ind1]= 1
            parent_vector_theo_label[ind0,ind1]= 1


            
        appo_leaves_label= parent_vector_label.flatten()
        appo_leaves = len(parent_vector.flatten()[appo_leaves_label[:]==2])
        
        i+=1

    m_leaves_label= parent_vector_label.flatten()
    m_leaves = parent_vector.flatten()[m_leaves_label[:]==2]

    #print(min(m_leaves))   
    #m_leaves= pick_up_vector[pick_up_vector_label[:]==2]

    x_leaves_label = x_leaves_label.flatten()
    y_leaves_label = y_leaves_label.flatten()
    z_leaves_label = z_leaves_label.flatten()

    vx_leaves_label = vx_leaves_label.flatten()
    vy_leaves_label = vy_leaves_label.flatten()
    vz_leaves_label = vz_leaves_label.flatten()

    x_leaves = x_leaves.flatten()[x_leaves_label[:]==2]
    y_leaves = y_leaves.flatten()[y_leaves_label[:]==2]
    z_leaves = z_leaves.flatten()[z_leaves_label[:]==2]

    vx_leaves = vx_leaves.flatten()[vx_leaves_label[:]==2]
    vy_leaves = vy_leaves.flatten()[vy_leaves_label[:]==2]
    vz_leaves = vz_leaves.flatten()[vz_leaves_label[:]==2]

    std_pos = np.sqrt((np.var(x_leaves)+np.var(y_leaves)+np.var(z_leaves))/3.)
    std_vel = np.sqrt((np.var(vx_leaves)+np.var(vy_leaves)+np.var(vz_leaves))/3.)
    print("Std leaves pos:", std_pos)
    print("Std leaves vel:", std_vel)

    x_leaves = x_leaves*scales_r[Nsim]/std_pos
    y_leaves = y_leaves*scales_r[Nsim]/std_pos
    z_leaves = z_leaves*scales_r[Nsim]/std_pos

    vx_leaves = vx_leaves*scales_v[Nsim]/std_vel
    vy_leaves = vy_leaves*scales_v[Nsim]/std_vel
    vz_leaves = vz_leaves*scales_v[Nsim]/std_vel


    #Plot the new created distributions
    new_splitted_m.flatten()

    plt.figure(figsize=(8,8))
    #print(new_splitted_m.flatten())
    print(len(m_leaves),len(m_gand_Nsim), np.sum(m_gand_Nsim), np.sum(m_leaves))
    plt.hist(m_gand_Nsim, bins=np.logspace(0.8*np.log10(min(min(m_leaves),min(m_gand_Nsim))),1.2*np.log10(max(max(m_leaves),max(m_gand_Nsim))),40),histtype='stepfilled',facecolor='None',edgecolor="Blue", label="Original")
    plt.hist(m_leaves,bins=np.logspace(0.8*np.log10(min(min(m_leaves),min(m_gand_Nsim))),1.2*np.log10(max(max(m_leaves),max(m_gand_Nsim))),40),histtype='stepfilled',facecolor='None',edgecolor="Orange", label="New")
    #print(new_mass_vector)
    plt.xlabel(r"$m [M_{\odot}]$")
    plt.xscale("log")
    plt.yscale("log")
    plt.legend()
    plt.savefig("./new_mass_splitted_distribution_seed"+str(seed)+"_"+str(Nsim)+".png")
    plt.close()
      

    pos_leaves= np.stack((x_leaves,y_leaves,z_leaves),axis=-1)
    pos_tree=spat.cKDTree(pos_leaves)
    denr=0.
   
    density_leaves = np.zeros(len(m_leaves))
    
    for i in np.arange(len(m_leaves)):
        indneigh=pos_tree.query(pos_leaves[i],k=500)[1]
        dist_tree = np.sqrt((x_leaves[indneigh]-x_leaves[i])**2.+(y_leaves[indneigh]-y_leaves[i])**2.+(z_leaves[indneigh]-z_leaves[i])**2.)
        
        den=len(m_leaves[indneigh])/((max(dist_tree))**3.)
        density_leaves[i] = den
        
    cm = plt.cm.get_cmap('viridis')

    if(count>=0 and count<3):
        plt.figure(10)
        if(count==0):
            plt.subplot(221)
            plt.hist(m_gand_Nsim, bins=np.logspace(np.log10(min(min(m_leaves),min(m_gand_Nsim))),np.log10(max(max(m_leaves),max(m_gand_Nsim))),40),histtype='stepfilled',facecolor='None',edgecolor="Blue", label="Original")
            plt.xlabel(r"$m [M_{\odot}]$")
            plt.xscale("log")
            plt.yscale("log")
            plt.legend()
        plt.subplot(2,2,count+2)
        plt.hist(m_leaves,bins=np.logspace(np.log10(min(min(m_leaves),min(m_gand_Nsim))),np.log10(max(max(m_leaves),max(m_gand_Nsim))),40),histtype='stepfilled',facecolor='None',edgecolor="Blue", label="Seed = "+str(seed))
        plt.xlabel(r"$m [M_{\odot}]$")
        plt.xscale("log")
        plt.yscale("log")
        plt.legend()
        if(count==2):
            plt.figure(10)
            plt.tight_layout()
            plt.savefig("./new_mass_combined"+"_"+str(Nsim)+".png")
            plt.close()

        plt.figure(11)
        if(count==0):
            plt.subplot(221)
            plt.scatter(x_gand_Nsim, y_gand_Nsim, c = density_gand, s=4, cmap=cm, norm=matplotlib.colors.LogNorm(), vmin=min(density_gand), vmax=max(density_gand), label="Original")
            plt.xlabel(r"$x [pc]$")
            plt.ylabel(r"$y [pc]$")
            plt.legend()
        plt.subplot(2,2,count+2)
        plt.scatter(x_leaves, y_leaves, c = density_leaves, s=4, cmap=cm, norm=matplotlib.colors.LogNorm(), vmin=min(density_gand), vmax=max(density_gand), label="Seed = "+str(seed))
        if(count==0 or count==2):
            plt.colorbar()
        plt.xlabel(r"$x [pc]$")
        plt.ylabel(r"$y [pc]$")
        plt.legend()
        if(count==2):
            plt.figure(11)
            plt.tight_layout()
            plt.savefig("./new_xy_combined_seed"+"_"+str(Nsim)+".png")
            plt.close()

        plt.figure(12)
        if(count==0):
            plt.subplot(221)
            plt.scatter(y_gand_Nsim, z_gand_Nsim, c = density_gand, s=4, cmap=cm, norm=matplotlib.colors.LogNorm(), vmin=min(density_gand), vmax=max(density_gand), label="Original")
            plt.xlabel(r"$y [pc]$")
            plt.ylabel(r"$z [pc]$")
            plt.legend()
        plt.subplot(2,2,count+2)
        plt.scatter(y_leaves, z_leaves, c = density_leaves, s=4, cmap=cm, norm=matplotlib.colors.LogNorm(), vmin=min(density_gand), vmax=max(density_gand), label="Seed = "+str(seed))
        if(count==0 or count==2):
            plt.colorbar()
        plt.xlabel(r"$y [pc]$")
        plt.ylabel(r"$z [pc]$")
        plt.legend()
        if(count==2):
            plt.figure(12)
            plt.tight_layout()
            plt.savefig("./new_yz_combined_seed"+"_"+str(Nsim)+".png")
            plt.close()

        plt.figure(13)
        if(count==0):
            plt.subplot(221)
            plt.scatter(z_gand_Nsim, x_gand_Nsim, c = density_gand, s=4, cmap=cm, norm=matplotlib.colors.LogNorm(), vmin=min(density_gand), vmax=max(density_gand), label="Original")
            plt.xlabel(r"$z [pc]$")
            plt.ylabel(r"$x [pc]$")
            plt.legend()
        plt.subplot(2,2,count+2)
        plt.scatter(z_leaves, x_leaves, c = density_leaves, s=4, cmap=cm, norm=matplotlib.colors.LogNorm(), vmin=min(density_gand), vmax=max(density_gand), label="Seed = "+str(seed))
        if(count==0 or count==2):
            plt.colorbar()
        plt.xlabel(r"$z [pc]$")
        plt.ylabel(r"$x [pc]$")
        plt.legend()
        if(count==2):
            plt.figure(13)
            plt.tight_layout()
            plt.savefig("./new_zx_combined_seed"+"_"+str(Nsim)+".png")
            plt.close()

        plt.figure(14)
        if(count==0):
            plt.subplot(221)
            plt.scatter(vx_gand_Nsim, vy_gand_Nsim, c = density_gand, s=10, cmap=cm, norm=matplotlib.colors.LogNorm(), vmin=min(density_gand), vmax=max(density_gand), label="Original")
            plt.xlabel(r"$v_x [km/s]$")
            plt.ylabel(r"$v_y [km/s]$")
            plt.legend()
        plt.subplot(2,2,count+2)
        plt.scatter(vx_leaves, vy_leaves, c = density_leaves, s=10, cmap=cm, norm=matplotlib.colors.LogNorm(), vmin=min(density_gand), vmax=max(density_gand), label="Seed = "+str(seed))
        if(count==0 or count==2):
            plt.colorbar()
        plt.xlabel(r"$v_x [km/s]$")
        plt.ylabel(r"$v_y [km/s]$")
        plt.legend()
        if(count==2):
            plt.figure(14)
            plt.tight_layout()
            plt.savefig("./new_vxvy_combined_seed"+"_"+str(Nsim)+".png")
            plt.close()

        plt.figure(15)
        if(count==0):
            plt.subplot(221)
            plt.scatter(vy_gand_Nsim, vz_gand_Nsim, c = density_gand, s=10, cmap=cm, norm=matplotlib.colors.LogNorm(), vmin=min(density_gand), vmax=max(density_gand), label="Original")
            plt.xlabel(r"$vy [km/s]$")
            plt.ylabel(r"$vz [km/s]$")
            plt.legend()
        plt.subplot(2,2,count+2)
        plt.scatter(vy_leaves, vz_leaves, c = density_leaves, s=10, cmap=cm, norm=matplotlib.colors.LogNorm(), vmin=min(density_gand), vmax=max(density_gand), label="Seed = "+str(seed))
        if(count==0 or count==2):
            plt.colorbar()
        plt.xlabel(r"$v_y [km/s]$")
        plt.ylabel(r"$v_z [km/s]$")
        plt.legend()
        if(count==2):
            plt.figure(15)
            plt.tight_layout()
            plt.savefig("./new_vyvz_combined_seed"+"_"+str(Nsim)+".png")
            plt.close()

        plt.figure(16)
        if(count==0):
            plt.subplot(221)
            plt.scatter(vz_gand_Nsim, vx_gand_Nsim, c = density_gand, s=10, cmap=cm, norm=matplotlib.colors.LogNorm(), vmin=min(density_gand), vmax=max(density_gand), label="Original")
            plt.xlabel(r"$v_z [km/s]$")
            plt.ylabel(r"$v_x [km/s]$")
            plt.legend()
        plt.subplot(2,2,count+2)
        plt.scatter(vz_leaves, vx_leaves, c = density_leaves, s=10, cmap=cm, norm=matplotlib.colors.LogNorm(), vmin=min(density_gand), vmax=max(density_gand), label="Seed = "+str(seed))
        if(count==0 or count==2):
            plt.colorbar()
        plt.xlabel(r"$v_z [km/s]$")
        plt.ylabel(r"$v_x [km/s]$")
        plt.legend()
        if(count==2):
            plt.figure(16)
            plt.tight_layout()
            plt.savefig("./new_vzvx_combined_seed"+"_"+str(Nsim)+".png")
            plt.close()

    
    appo = np.linspace(1,len(new_node_mass_vector)+1,len(new_node_mass_vector))
    #appo = appo[::-1]

    plt.figure(40)
    plt.plot(appo, new_node_mass_vector, lw=0.00001, alpha=0.5, marker=".", color=colorv[1], label="New")

    plt.xlabel(r'$N_{splitting}$') 
    plt.ylabel(r'$m_{node}$') 
    plt.xscale('log')
    plt.yscale('log')
    #plt.ylim(0.0002,1.1)
    plt.legend()
    plt.tight_layout()     
    plt.savefig("./new_node_mass_s"+str(seed)+"_"+str(Nsim)+".png") 
    plt.close()
    count+=1


    fig4=plt.figure(figsize=(16,10))
    fig4.add_subplot(231)
    plt.hist(x_gand_Nsim, bins=np.linspace(min(x_gand_Nsim), max(x_gand_Nsim),40), weights=m_gand_Nsim, histtype='stepfilled',facecolor='None',edgecolor="Blue", label="Original")
    plt.hist(x_leaves, bins=np.linspace(min(x_gand_Nsim), max(x_gand_Nsim),40), weights=m_leaves, histtype='stepfilled',facecolor='None',edgecolor="Orange", label="New")
    plt.xlabel(r"$x [pc]$")
    plt.legend()

    fig4.add_subplot(232)
    plt.hist(y_gand_Nsim, bins=np.linspace(min(y_gand_Nsim), max(y_gand_Nsim),40), weights=m_gand_Nsim,  histtype='stepfilled',facecolor='None',edgecolor="Blue", label="Original")
    plt.hist(y_leaves, bins=np.linspace(min(y_gand_Nsim), max(y_gand_Nsim),40), weights=m_leaves,histtype='stepfilled',facecolor='None',edgecolor="Orange", label="New")
    plt.xlabel(r"$y [pc]$")
    plt.legend()

    fig4.add_subplot(233)
    plt.hist(z_gand_Nsim, bins=np.linspace(min(z_gand_Nsim), max(z_gand_Nsim),40), weights=m_gand_Nsim,  histtype='stepfilled',facecolor='None',edgecolor="Blue", label="Original")
    plt.hist(z_leaves, bins=np.linspace(min(z_gand_Nsim), max(z_gand_Nsim),40), weights=m_leaves, histtype='stepfilled',facecolor='None',edgecolor="Orange", label="New")
    plt.xlabel(r"$z [pc]$")
    plt.legend()

    fig4.add_subplot(234)
    plt.hist(vx_gand_Nsim, bins=np.linspace(min(vx_gand_Nsim), max(vx_gand_Nsim),40), weights=m_gand_Nsim,  histtype='stepfilled',facecolor='None',edgecolor="Blue", label="Original")
    plt.hist(vx_leaves, bins=np.linspace(min(vx_gand_Nsim), max(vx_gand_Nsim),40),weights=m_leaves, histtype='stepfilled',facecolor='None',edgecolor="Orange", label="New")
    plt.xlabel(r"$v_x [km/s]$")
    plt.legend()

    fig4.add_subplot(235)
    plt.hist(vy_gand_Nsim, bins=np.linspace(min(vy_gand_Nsim), max(vy_gand_Nsim),40), weights=m_gand_Nsim,  histtype='stepfilled',facecolor='None',edgecolor="Blue", label="Original")
    plt.hist(vy_leaves, bins=np.linspace(min(vy_gand_Nsim), max(vy_gand_Nsim),40),weights=m_leaves, histtype='stepfilled',facecolor='None',edgecolor="Orange", label="New")
    plt.xlabel(r"$v_y [km/s]$")
    plt.legend()

    fig4.add_subplot(236)
    plt.hist(vz_gand_Nsim, bins=np.linspace(min(vz_gand_Nsim), max(vz_gand_Nsim),40), weights=m_gand_Nsim,  histtype='stepfilled',facecolor='None',edgecolor="Blue", label="Original")
    plt.hist(vz_leaves, bins=np.linspace(min(vz_gand_Nsim), max(vz_gand_Nsim),40),weights=m_leaves, histtype='stepfilled',facecolor='None',edgecolor="Orange", label="New")
    plt.xlabel(r"$v_z [km/s]$")
    plt.legend()

    #plt.xscale("log")
    #plt.yscale("log")
    plt.savefig("./new_pos_vel_splitted_distribution_seed"+str(seed)+"_"+str(Nsim)+".png")
    plt.close()



    fig4=plt.figure(figsize=(16,10))
    fig4.add_subplot(231)
    plt.scatter(x_gand_Nsim, y_gand_Nsim, color="Blue",s=1, label="Original")
    plt.scatter(x_leaves, y_leaves, color="Orange",s=1, label="New")
    plt.xlabel(r"$x [pc]$")
    plt.ylabel(r"$y [pc]$")
    plt.legend()


    fig4.add_subplot(232)
    plt.scatter(y_gand_Nsim, z_gand_Nsim, color="Blue",s=1, label="Original")
    plt.scatter(y_leaves, z_leaves, color="Orange",s=1, label="New")
    plt.xlabel(r"$y [pc]$")
    plt.ylabel(r"$z [pc]$")
    plt.legend()


    fig4.add_subplot(233)
    plt.scatter(z_gand_Nsim, x_gand_Nsim, color="Blue",s=1, label="Original")
    plt.scatter(z_leaves, x_leaves, color="Orange",s=1, label="New")
    plt.xlabel(r"$z [pc]$")
    plt.ylabel(r"$x [pc]$")
    plt.legend()


    fig4.add_subplot(234)
    plt.scatter(vx_gand_Nsim, vy_gand_Nsim, color="Blue",s=1, label="Original")
    plt.scatter(vx_leaves, vy_leaves, color="Orange",s=1, label="New")
    plt.xlabel(r"$v_x [km/s]$")
    plt.ylabel(r"$v_y [km/s]$")
    plt.legend()


    fig4.add_subplot(235)
    plt.scatter(vy_gand_Nsim, vz_gand_Nsim, color="Blue",s=1, label="Original")
    plt.scatter(vy_leaves, vz_leaves, color="Orange",s=1, label="New")
    plt.xlabel(r"$v_y [km/s]$")
    plt.ylabel(r"$v_z [km/s]$")
    plt.legend()

    fig4.add_subplot(236)
    plt.scatter(vz_gand_Nsim, vx_gand_Nsim, color="Blue",s=1, label="Original")
    plt.scatter(vz_leaves, vx_leaves, color="Orange",s=1, label="New")
    plt.xlabel(r"$v_z [km/s]$")
    plt.ylabel(r"$v_x [km/s]$")
    plt.legend()



    #plt.xscale("log")
    #plt.yscale("log")
    plt.savefig("./new_pos_vel_scatter_distribution_seed"+str(seed)+"_"+str(Nsim)+".png")
    plt.close()

    fname='new_stars_seed_'+str(seed)+"_"+str(Nsim)+'.dat'
    f=open(fname,'w') 
    f.write("# n,  m [Msun], x [pc], y [pc], z [pc], vx [km/s], vy [km/s], vz [km/s]  \n")
    for j in range(len(m_leaves)):
        f.write("%.20f %.20f %.20f %.20f %.20f %.20f %.20f\n" % (m_leaves[j],x_leaves[j], y_leaves[j], z_leaves[j], vx_leaves[j], vy_leaves[j], vz_leaves[j]))


    Eksinkv = []
    Egsinkv = []

    for j in range(len(m_leaves)):
        Eksinkv.append(1./2.*m_leaves[j]*(vx_leaves[j]**2+vy_leaves[j]**2+vz_leaves[j]**2))
        mvectappo = np.delete(m_leaves,j)
        xvectappo = np.delete(x_leaves,j)
        yvectappo = np.delete(y_leaves,j)
        zvectappo = np.delete(z_leaves,j)
        vxvectappo = np.delete(vx_leaves,j)
        vyvectappo = np.delete(vy_leaves,j)
        vzvectappo = np.delete(vz_leaves,j)
        
        Egsinkv.append(np.sum(-0.5*G*m_leaves[j]*mvectappo /np.sqrt((x_leaves[j]-xvectappo)**2+(y_leaves[j]-yvectappo)**2+(z_leaves[j]-zvectappo)**2)))
        
    Eksink = sum(Eksinkv)
    Egsink = abs(sum(Egsinkv))
    vratio_leaves = 2.*Eksink/Egsink

    print(vratio_sink_Nsim,vratio_leaves)