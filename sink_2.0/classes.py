import numpy as np
import itertools
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from mpl_toolkits.mplot3d import Axes3D
import os
import scipy.spatial as spat
from scipy.spatial import KDTree

from functions import *



class cluster:
    
    def __init__(self, *args):
        
        if(isinstance(args[0],str)):
            try:
                self.m, self.x, self.y, self.z, self.vx, self.vy, self.vz = load_dat(args[0], args[1])
            except:
                self.m, self.x, self.y, self.z, self.vx, self.vy, self.vz = load_dat(args[0])           

        else:
            if(isinstance(args[0], list)):
                self.m, self.x, self.y, self.z, self.vx, self.vy, self.vz = np.array(args[0]), np.array(args[1]), np.array(args[2]), np.array(args[3]), np.array(args[4]), np.array(args[5])
            else:
                self.m, self.x, self.y, self.z, self.vx, self.vy, self.vz = args[0], args[1], args[2], args[3], args[4], args[5], args[6]
        
        self.Mtot = np.sum(self.m)
        self.d_var = np.sqrt((np.var(self.x)+np.var(self.y)+np.var(self.z))/3.)
        self.dv_var = np.sqrt((np.var(self.vx)+np.var(self.vy)+np.var(self.vz))/3.)
         

    def find_com(self, verbose=True):
        
        self.x_cm = np.sum(self.m*self.x) / self.Mtot
        self.y_cm = np.sum(self.m*self.y) / self.Mtot
        self.z_cm = np.sum(self.m*self.z) / self.Mtot
        self.vx_cm = np.sum(self.m*self.vx) / self.Mtot
        self.vy_cm = np.sum(self.m*self.vy) / self.Mtot
        self.vz_cm = np.sum(self.m*self.vz) / self.Mtot

        if(verbose==True):
            print("Center of mass position: ", self.x_cm, " [pc] ", self.y_cm, " [pc] ", self.z_cm, " [pc] ")
            print("Center of mass velocity: ", self.vx_cm, " [km/s] ", self.vy_cm, " [km/s] ", self.vz_cm, " [km/s] ")
 

    def find_and_rescale_com(self, verbose=True):

        self.find_com()

        #Rescale the positions and velocities in their c.o.m.

        self.x = self.x - self.x_cm
        self.y = self.y - self.y_cm
        self.z = self.z - self.z_cm
        self.vx = self.vx - self.vx_cm
        self.vy = self.vy - self.vy_cm
        self.vz = self.vz - self.vz_cm


    def energies(self):
        
        Eg_vect = np.zeros(len(self.m))

        for j in range(len(self.m)):
            mvectappo = np.delete(self.m,j)
            xvectappo = np.delete(self.x,j)
            yvectappo = np.delete(self.y,j)
            zvectappo = np.delete(self.z,j)
            vxvectappo = np.delete(self.vx,j)
            vyvectappo = np.delete(self.vy,j)
            vzvectappo = np.delete(self.vz,j)

            Eg_vect[j] = np.sum(-0.5*G*self.m[j]*mvectappo /np.sqrt((self.x[j]-xvectappo)**2.+(self.y[j]-yvectappo)**2.+(self.z[j]-zvectappo)**2.))

        self.Ek = np.sum(0.5*self.m*(self.vx**2.+self.vy**2.+self.vz**2.))
        self.Eg = abs(np.sum(Eg_vect))

        self.vratio = 2.*self.Ek/self.Eg

        self.v_radius = G*np.sum(self.m)**2 /(2.*self.Eg)
        self.t_scale = np.sqrt(self.v_radius**3./(G*self.Mtot))
        self.v_scale = self.v_radius / np.sqrt(self.v_radius**3. / (G*self.Mtot))


    def density(k=500):

        pos = np.stack((self.x, self.y, self.z),axis=-1)
        pos_tree=spat.cKDTree(pos)
        denr=0.

        self.density = np.zeros(len(self.m))

        for j in np.arange(len(self.m)):
            indneigh=pos_tree.query(pos[j],k)[1]
            dist_tree = np.sqrt((self.x[indneigh]-self.x[j])**2.+(self.y[indneigh]-self.y[j])**2.+(self.z[indneigh]-self.z[j])**2.)

            den=np.sum(self.m[indneigh])/(4./3.*np.pi*(max(dist_tree))**3.)
            self.density[j] = den

    
    
    def distances(self):

        self.dist = np.zeros((len(self.m),len(self.m)-1))

        for j in range(len(self.m)):
            xvectappo = np.delete(self.x,j)
            yvectappo = np.delete(self.y,j)
            zvectappo = np.delete(self.z,j)

            self.dist[j]=np.sqrt((self.x[j]-xvectappo)**2+(self.y[j]-yvectappo)**2+(self.z[j]-zvectappo)**2) #evaluate the distribution of distances

        
    def neighbors(self, binning):
        pos= np.stack((self.x, self.y, self.z),axis=-1)
        pos_tree=spat.cKDTree(pos)

        neig = np.zeros(len(binning))
        self.neigh = np.zeros(len(binning))

        for j in np.arange(len(self.m)):
            for n in range(len(binning)):
                indneigh=len(pos_tree.query_ball_point(pos[j],r=binning[n]))-1
                neig[n]+= indneigh

        self.neigh = neig/(len(self.m)-1)
        
        
    def print_out(self, file_name, out_extension="dat", units="astro"):
        fname = os.path.join(OUTPUT_PATH, file_name + "." + out_extension)
        f=open(fname,'w') 
        
        if(units=="astro"):
            f.write("# m [Msun], x [pc], y [pc], z [pc], vx [km/s], vy [km/s], vz [km/s]  \n")
            for j in range(len(self.m)):
                f.write("%.10f %.10f %.10f %.10f %.10f %.10f %.10f\n" % (self.m[j], self.x[j], self.y[j], self.z[j], self.vx[j], self.vy[j], self.vz[j]))
        else:
            f.write("# m, x, y, z, vx, vy, vz [Nbody units]  \n")
            for j in range(len(self.m)):
                f.write("%.10f %.10f %.10f %.10f %.10f %.10f %.10f\n" % (self.m[j]/self.Mtot, self.x[j]/self.v_radius, self.y[j]/self.v_radius, self.z[j]/self.v_radius, self.vx[j]/self.v_scale, self.vy[j]/self.v_scale, self.vz[j]/self.v_scale))
