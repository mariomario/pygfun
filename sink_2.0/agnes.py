from sklearn.cluster import AgglomerativeClustering, ward_tree
import numpy as np
import itertools
import matplotlib
matplotlib.use('Agg') 
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from scipy.stats import ks_2samp
from mpl_toolkits.mplot3d import Axes3D
from scipy.cluster.hierarchy import linkage, fcluster, dendrogram
import os
import yt


G=0.00430145 #gravitational constant in astronomical units [pc]*[MSun]**(-1)*[km/s]**2 (the same as Nbody6++GPU)

#Main
# It read sinks at a time t

t=3.0

gandnum=t*10.+1.
gandlab='{:02.0f}'.format(gandnum)

gasnum=t*100.
gaslab='{:03.0f}'.format(gasnum)

#GANDALF READING

x_gand=[]
y_gand=[]
z_gand=[]
vx_gand=[]
vy_gand=[]
vz_gand=[]
m_gand=[]

#GASOLINE READING, THROUGH YT

path = '/tank0/ballone/hydro_set_mcs_orig'

files = []
files2 = []
# r=root, d=directories, f = files
for r, d, f in os.walk(path):
    for file in f:
        if 'out.00390' in file and '.den' not in file: 
                files.append(os.path.join(r, file))
                print(files[-1])

numbers = np.array([1,10,2,3,4,5,6,7,8,9])
files.sort()
files_array = np.column_stack((numbers, files))
files_array = sorted(files_array, key = lambda x: float(x[0])) 
#print(files_array)
for i in range(len(files)):
    files[i] = files_array[i][1]
#files = np.sort(files)
#print(files)

conv=2.22249e2 
labelsv = []  
nodes_ratios_vector = [] 
nodes_vector_vector = []
nodes_vector1_vector = []
nodes_vector2_vector = []
nodes_vector3_vector = []
nodes_vector4_vector = []
nodes_vector5_vector = []

dxcm_vector = []
dycm_vector = []
dzcm_vector = []
dcm_vector = []

dvxcm_vector = []
dvycm_vector = []
dvzcm_vector = []
dvcm_vector = []

theta_vector = []
nodes_plot_vector = []

scales_m = []
scales_r = []
scales_v = []

#files = files[:5]
#fig1= plt.figure(figsize=(10,10))
Ni = 1
colorv = ['orange','red','blue','green',"black","yellow","cyan","magenta","brown","pink"]
for f in files:
    #GANDALF READING

    x_gand=[]
    y_gand=[]
    z_gand=[]
    vx_gand=[]
    vy_gand=[]
    vz_gand=[]
    m_gand=[]

    #GASOLINE READING, THROUGH YT

    simname = f
    simnamered = str(f[34:39])
    labelsv.append(simnamered)
    print(simnamered)

    ds=yt.load(f)
    dd=ds.all_data()

    #convert the mass from n-body units to solar
    m_gand=dd[('Stars',"Mass")]*conv
    x_gand=dd[('Stars',"particle_position_x")]
    y_gand=dd[('Stars',"particle_position_y")]
    z_gand=dd[('Stars',"particle_position_z")]
    vx_gand=dd[('Stars',"particle_velocity_x")]
    vy_gand=dd[('Stars',"particle_velocity_y")]
    vz_gand=dd[('Stars',"particle_velocity_z")]

    #determine the center of mass of the system
    x_cm_sink = np.sum(x_gand*m_gand) /np.sum(m_gand)
    y_cm_sink = np.sum(y_gand*m_gand) /np.sum(m_gand)
    z_cm_sink = np.sum(z_gand*m_gand) /np.sum(m_gand)
    vx_cm_sink = np.sum(vx_gand*m_gand) /np.sum(m_gand)
    vy_cm_sink = np.sum(vy_gand*m_gand) /np.sum(m_gand)
    vz_cm_sink = np.sum(vz_gand*m_gand) /np.sum(m_gand)

    #rescale all the positions and velocities to those of the c.o.m.
    print "Sink center of mass position: ", x_cm_sink, " [pc] ", y_cm_sink, " [pc] ", z_cm_sink, " [pc] "
    print "Sink center of mass velocity: ", vx_cm_sink, " [km/s] ", vy_cm_sink, " [km/s] ", vz_cm_sink, " [km/s] "
    x_gand = x_gand - x_cm_sink
    y_gand = y_gand - y_cm_sink
    z_gand = z_gand - z_cm_sink
    vx_gand = vx_gand - vx_cm_sink
    vy_gand = vy_gand - vy_cm_sink
    vz_gand = vz_gand - vz_cm_sink


    m=np.array(m_gand)
    #evaluate the position and velocity dispersions
    d_var = np.sqrt((np.var(x_gand)+np.var(y_gand)+np.var(z_gand))/3.)
    dv_var = np.sqrt((np.var(vx_gand)+np.var(vy_gand)+np.var(vz_gand))/3.)
    
    scales_m.append(np.sum(m_gand))
    scales_r.append(d_var)
    scales_v.append(dv_var)

    # We want to make a 6-dim tree. 
    # We first need to rescale the positions and velocities to their dispersions. 
    X=np.column_stack((x_gand/d_var,y_gand/d_var,z_gand/d_var,vx_gand/dv_var,vy_gand/dv_var,vz_gand/dv_var))
    V=np.column_stack((vx_gand/dv_var,vy_gand/dv_var,vz_gand/dv_var))
    i_sort=np.argsort(m_gand)
    print(np.sum(m_gand))

    #Use AgglomerativeClustering to create the tree
    clustering = AgglomerativeClustering(n_clusters=X.shape[0],linkage="ward").fit(X) #n_clusters=X.shape[0] to make the full tree

   
    
    node_mass = np.zeros(X.shape[0]-1)  #vector of nodes: each element of this vector corresponds to the sum of the masses of its leaves/sub-branches. I will use it later to calculate the mass ratios

    for i in range(X.shape[0]-1):
        for j in range(2):
            if(clustering.children_[i][j]<X.shape[0]): #we are in the regime of the leaves
                node_mass[i]+=m[clustering.children_[i][j]] 
            else: #we are in the regime of the branches, so we need to sum over the sub-branches/leaves
                node_mass[i]+=node_mass[clustering.children_[i][j]-X.shape[0]]


   
    nodes_ratios = np.zeros([X.shape[0]-1,2])  #every element of this vector will be the ratio between a branch and the node it comes from
    nodes_ratios_vector.append(nodes_ratios[:,1])

    mcm = np.zeros([X.shape[0]-1,2])

    for i in range(X.shape[0]-1):
        for j in range(2):
            if(clustering.children_[i][j]<X.shape[0]):  #we are in the regime of the leaves
                nodes_ratios[i][j]+=m[clustering.children_[i][j]]/node_mass[i]
                mcm[i][j]+=m[clustering.children_[i][j]]
            else: #we are in the regime of the branches
                nodes_ratios[i][j]+=node_mass[clustering.children_[i][j]-X.shape[0]]/node_mass[i]
                mcm[i][j]+=node_mass[clustering.children_[i][j]-X.shape[0]]

    nodes_plot = []
    

    #Differences of mass ratios (we will use it for some plots)
    for i in range(len(nodes_ratios)):
        nodes_plot.append(max(nodes_ratios[i])-min(nodes_ratios[i])) 
    
    nodes_plot_vector.append(nodes_plot)
    
    nodes_vector=np.array(nodes_ratios.flatten())
    nodes_vector_vector.append(nodes_vector)

    #consider different levels of the tree
    nodes_vector1 = nodes_vector[-10:] 
    nodes_vector2 = nodes_vector[-100:] 
    nodes_vector3 = nodes_vector[:50] 
    nodes_vector4 = nodes_vector[:500] 
    nodes_vector5 = nodes_vector[:2000] 

    nodes_vector1_vector.append(nodes_vector1)
    nodes_vector2_vector.append(nodes_vector2)
    nodes_vector3_vector.append(nodes_vector3)
    nodes_vector4_vector.append(nodes_vector4)
    nodes_vector5_vector.append(nodes_vector5)

    #
    # Evaluate the distances between the c.o.m. of the leaves/branches
    #

    #first we evaluate the positions of the c.o.m.
    xcm = np.zeros([X.shape[0]-1,2])
    ycm = np.zeros([X.shape[0]-1,2])
    zcm = np.zeros([X.shape[0]-1,2])

    for i in range(X.shape[0]-1):
        for j in range(2):
            if(clustering.children_[i][j]<X.shape[0]): #for the leaves, the c.o.m. is the position of the stars
                xcm[i][j]+=X[clustering.children_[i][j]][0]
                ycm[i][j]+=X[clustering.children_[i][j]][1]
                zcm[i][j]+=X[clustering.children_[i][j]][2]
            else:	#for the branches, we sum over the leaves/subbranches
                xcm[i][j]+=np.sum(mcm[clustering.children_[i][j]-X.shape[0]]*xcm[clustering.children_[i][j]-X.shape[0]])/np.sum(mcm[clustering.children_[i][j]-X.shape[0]])
                ycm[i][j]+=np.sum(mcm[clustering.children_[i][j]-X.shape[0]]*ycm[clustering.children_[i][j]-X.shape[0]])/np.sum(mcm[clustering.children_[i][j]-X.shape[0]])
                zcm[i][j]+=np.sum(mcm[clustering.children_[i][j]-X.shape[0]]*zcm[clustering.children_[i][j]-X.shape[0]])/np.sum(mcm[clustering.children_[i][j]-X.shape[0]])


    dcm = np.zeros(X.shape[0]-1)
    dxcm = np.zeros(X.shape[0]-1)
    dycm = np.zeros(X.shape[0]-1)
    dzcm = np.zeros(X.shape[0]-1)	
    dxcm1 = np.zeros(X.shape[0]-1)
    dycm1 = np.zeros(X.shape[0]-1)
    dzcm1 = np.zeros(X.shape[0]-1)	

    #modules of the distances of c.o.m.
    for i in range(X.shape[0]-1):
        dxcm[i]+=abs(xcm[i][0]-xcm[i][1])
        dycm[i]+=abs(ycm[i][0]-ycm[i][1])
        dzcm[i]+=abs(zcm[i][0]-zcm[i][1])

        dcm[i]+=np.sqrt(dxcm[i]**2+dycm[i]**2+dzcm[i]**2)

    for i in range(X.shape[0]-1):
        dxcm1[i]+=xcm[i][0]-xcm[i][1]
        dycm1[i]+=ycm[i][0]-ycm[i][1]
        dzcm1[i]+=zcm[i][0]-zcm[i][1]

 
    dxcm_vector.append(dxcm)
    dycm_vector.append(dycm)
    dzcm_vector.append(dzcm)
    dcm_vector.append(dcm)

    #
    # Evaluate the distances between the c.o.m. velocities of the leaves/branches (same procedure as for the positions)
    #


    vxcm = np.zeros([V.shape[0]-1,2])
    vycm = np.zeros([V.shape[0]-1,2])
    vzcm = np.zeros([V.shape[0]-1,2])
    for i in range(V.shape[0]-1):
        for j in range(2):
            if(clustering.children_[i][j]<V.shape[0]):
                vxcm[i][j]+=V[clustering.children_[i][j]][0]
                vycm[i][j]+=V[clustering.children_[i][j]][1]
                vzcm[i][j]+=V[clustering.children_[i][j]][2]
                #nodes_ratios1[i][j]+=m[clustering.children_[i][j]]/(node_mass[i]-m[clustering.children_[i][j]])
            else:	
                vxcm[i][j]+=np.sum(mcm[clustering.children_[i][j]-V.shape[0]]*vxcm[clustering.children_[i][j]-V.shape[0]])/np.sum(mcm[clustering.children_[i][j]-V.shape[0]])
                vycm[i][j]+=np.sum(mcm[clustering.children_[i][j]-V.shape[0]]*vycm[clustering.children_[i][j]-V.shape[0]])/np.sum(mcm[clustering.children_[i][j]-V.shape[0]])
                vzcm[i][j]+=np.sum(mcm[clustering.children_[i][j]-V.shape[0]]*vzcm[clustering.children_[i][j]-V.shape[0]])/np.sum(mcm[clustering.children_[i][j]-V.shape[0]])

    dvcm = np.zeros(V.shape[0]-1)
    dvxcm = np.zeros(V.shape[0]-1)
    dvycm = np.zeros(V.shape[0]-1)
    dvzcm = np.zeros(V.shape[0]-1)

    dvxcm1 = np.zeros(V.shape[0]-1)
    dvycm1 = np.zeros(V.shape[0]-1)
    dvzcm1 = np.zeros(V.shape[0]-1)	
	

    for i in range(V.shape[0]-1):
        dvxcm[i]+=abs(vxcm[i][0]-vxcm[i][1])
        dvycm[i]+=abs(vycm[i][0]-vycm[i][1])
        dvzcm[i]+=abs(vzcm[i][0]-vzcm[i][1])

        dvcm[i]+=np.sqrt(dvxcm[i]**2+dvycm[i]**2+dvzcm[i]**2)

    for i in range(V.shape[0]-1):
        dvxcm1[i]+=vxcm[i][0]-vxcm[i][1]
        dvycm1[i]+=vycm[i][0]-vycm[i][1]
        dvzcm1[i]+=vzcm[i][0]-vzcm[i][1]

    #dvcm = dvcm/dvcm[X.shape[0]-2]
    dvxcm_vector.append(dvxcm)
    dvycm_vector.append(dvycm)
    dvzcm_vector.append(dvzcm)
    dvcm_vector.append(dvcm)

    #Evaluate the cosine of the angles between the c.o.m.

    theta = np.zeros(V.shape[0]-1)
    
    for i in range(len(dvcm)):
        theta[i] = (dxcm1[i]*dvxcm1[i] + dycm1[i]*dvycm1[i] + dzcm1[i]*dvzcm1[i]) / (dcm[i]*dvcm[i])  
        #print theta[i]

    theta_vector.append(theta)

    Ni+=1
    

nodes_vector_vector = np.array(nodes_vector_vector)
nodes_vector1_vector = np.array(nodes_vector1_vector)
nodes_vector2_vector = np.array(nodes_vector2_vector)
nodes_vector3_vector = np.array(nodes_vector3_vector)
nodes_vector4_vector = np.array(nodes_vector4_vector)
nodes_vector5_vector = np.array(nodes_vector5_vector)


#
# Plot the distributions
#

cmap = matplotlib.cm.get_cmap('viridis')

plt.rc('xtick',labelsize=14)
plt.rc('ytick',labelsize=14) 

#Mass ratios distributions
#Plot the mass ratios distributions at different levels of the tree
fig2= plt.figure(figsize=(12,10))

for i in range(0,len(nodes_vector_vector),2):

    weights= np.zeros(len(nodes_vector_vector[i]))+1./float(len(nodes_vector_vector[i]))
    weights1= np.zeros(len(nodes_vector1_vector[i]))+1./float(len(nodes_vector1_vector[i]))
    weights2= np.zeros(len(nodes_vector2_vector[i]))+1./float(len(nodes_vector2_vector[i]))
    weights3= np.zeros(len(nodes_vector3_vector[i]))+1./float(len(nodes_vector3_vector[i]))
    weights4= np.zeros(len(nodes_vector4_vector[i]))+1./float(len(nodes_vector4_vector[i]))
    weights5= np.zeros(len(nodes_vector5_vector[i]))+1./float(len(nodes_vector5_vector[i]))

    rgba = cmap(np.log10(0.5+1.25*i))


    fig2.add_subplot(221)
    plt.hist(nodes_vector_vector[i], weights=weights, bins=np.linspace(0,1, 16),  histtype='stepfilled', facecolor='None', edgecolor=rgba, linewidth=2.)
    plt.title('All splittings', fontsize=16)
    plt.xlabel(r'$m_{branch}/m_{node}$', fontsize=16)
    fig2.add_subplot(222)
    #plt.ylabel(r'$m_{leave}/m_{node}$')
    plt.title('First 2000 splittings', fontsize=16)
    plt.hist(nodes_vector5_vector[i], weights=weights5, bins=np.linspace(0,1, 16),  histtype='stepfilled', facecolor='None', edgecolor=rgba, linewidth=2.)
    plt.xlabel(r'$m_{branch}/m_{node}$', fontsize=16)
    fig2.add_subplot(223)
    #plt.ylabel(r'$m_{leave}/m_{node}$')
    plt.title('First 500 splittings', fontsize=16)
    plt.hist(nodes_vector4_vector[i], weights=weights4, bins=np.linspace(0,1, 16),  histtype='stepfilled', facecolor='None', edgecolor=rgba, linewidth=2.)
    plt.xlabel(r'$m_{branch}/m_{node}$', fontsize=16)
    fig2.add_subplot(224)
    plt.title('First 50 splittings', fontsize=16)
    plt.hist(nodes_vector3_vector[i], weights=weights3, bins=np.linspace(0,1, 16),  histtype='stepfilled', facecolor='None', edgecolor=rgba, label=labelsv[i], linewidth=2.)
    plt.xlabel(r'$m_{branch}/m_{node}$', fontsize=16) 
    plt.legend(fontsize=18)

"""
plt.legend(labelsv[::2],     # The line objects
    borderaxespad=0.1,    # Small spacing around legend box
    loc="center right",
    fontsize=16
        )       
"""   
plt.legend()
plt.tight_layout()
fig2.savefig('mass_splitting_histograms.png')

stat_vector = []
p_vector = []


#Kolgoromov-Smirnov test for the mass ratios
for i in range(len(nodes_ratios_vector)):
    for j in range(i+1,len(nodes_ratios_vector)):
        stat_vector.append(ks_2samp(nodes_ratios_vector[i],nodes_ratios_vector[j])[0])
        p_vector.append(ks_2samp(nodes_ratios_vector[i],nodes_ratios_vector[j])[1])


#p-value plot
fig4= plt.figure(figsize=(10,10))
plt.hist(p_vector, bins=np.logspace(np.log10(min(p_vector)),np.log10(max(p_vector)),40))
plt.xlabel('pvalue')
plt.ylabel('count')
plt.xscale('log')

plt.savefig("ks_test_mass.png")
#plt.show()
plt.close()



#Plot the distributions of c.o.m. distances for every component and for the total distance

dxcm_comp = []
dycm_comp = []
dzcm_comp = []
dcm_comp = []

fig3= plt.figure(figsize=(12,10))

for i in range(0,len(dcm_vector),2):

    weights= np.zeros(len(dcm_vector[i]))+1./float(len(dcm_vector[i]))
    
    rgba = cmap(np.log10(0.5+1.25*i))

    fig3.add_subplot(221)
    plt.hist(dxcm_vector[i], bins=np.logspace(np.log10(min(dxcm_vector[i])),np.log10(max(dxcm_vector[i])),16), weights = weights,  histtype='stepfilled', facecolor='None', edgecolor=rgba, linewidth=2.)
    plt.xlabel(r'$\Delta x_{cm}$', fontsize=18)
    plt.xscale('log')
    fig3.add_subplot(222)
    plt.hist(dycm_vector[i], bins=np.logspace(np.log10(min(dycm_vector[i])),np.log10(max(dycm_vector[i])),16), weights = weights,  histtype='stepfilled', facecolor='None', edgecolor=rgba, linewidth=2.)
    plt.xlabel(r'$\Delta y_{cm}$', fontsize=18)
    plt.xscale('log')
    fig3.add_subplot(223)
    plt.hist(dzcm_vector[i], bins=np.logspace(np.log10(min(dzcm_vector[i])),np.log10(max(dzcm_vector[i])),16), weights = weights,  histtype='stepfilled', facecolor='None', edgecolor=rgba, linewidth=2.)
    plt.xlabel(r'$\Delta z_{cm}$', fontsize=18)
    plt.xscale('log')
    fig3.add_subplot(224)
    plt.hist(dcm_vector[i], bins=np.logspace(np.log10(min(dcm_vector[i])),np.log10(max(dcm_vector[i])),16), weights = weights,  histtype='stepfilled', facecolor='None', label=labelsv[i], edgecolor=rgba, linewidth=2.)
    plt.xlabel(r'$d_{cm}$', fontsize=18)
    plt.xscale('log')   
    plt.legend(fontsize=18, loc='upper left')



"""
fig3.legend(labelsv[::2],     # The line objects
           loc="center right",   # Position of legend
           borderaxespad=0.1,    # Small spacing around legend box
           fontsize=16
           )       
plt.legend()
"""
#fig1.savefig('sink_distributions.png')
plt.tight_layout()
fig3.savefig('cm_histograms.png')

#Kolgoromov-Smirnov test for the c.o.m. distances

for i in range(len(dxcm_vector)):
    for j in range(i+1,len(dxcm_vector)):
        dxcm_comp.append(ks_2samp(dxcm_vector[i],dxcm_vector[j])[1])
        dycm_comp.append(ks_2samp(dycm_vector[i],dycm_vector[j])[1])
        dzcm_comp.append(ks_2samp(dzcm_vector[i],dzcm_vector[j])[1])
        dcm_comp.append(ks_2samp(dcm_vector[i],dcm_vector[j])[1])

fig5= plt.figure(figsize=(16,12))

fig5.add_subplot(221)
plt.hist(dxcm_comp, bins=np.logspace(np.log10(min(dxcm_comp)),np.log10(max(dxcm_comp)),40))
plt.xlabel('pvalue - xcm')
plt.xscale('log')
fig5.add_subplot(222)
plt.hist(dycm_comp, bins=np.logspace(np.log10(min(dycm_comp)),np.log10(max(dycm_comp)),40))
plt.xlabel('pvalue - ycm')
plt.xscale('log')
fig5.add_subplot(223)
plt.hist(dzcm_comp, bins=np.logspace(np.log10(min(dzcm_comp)),np.log10(max(dzcm_comp)),40))
plt.xlabel('pvalue - zcm')
plt.xscale('log')
fig5.add_subplot(224)
plt.hist(dcm_comp, bins=np.logspace(np.log10(min(dcm_comp)),np.log10(max(dcm_comp)),40))
plt.xlabel('pvalue - cm')
plt.xscale('log')   

plt.savefig("ks_test_cm.png")
#plt.show()
plt.close()

#Plot the distributions of c.o.m. velocity distances for every component and for the total distance


fig6= plt.figure(figsize=(12,10))

for i in range(0,len(dvcm_vector),2):

    weights= np.zeros(len(dvcm_vector[i]))+1./float(len(dvcm_vector[i]))

    rgba = cmap(np.log10(0.5+1.25*i))

    fig6.add_subplot(221)
    plt.hist(dvxcm_vector[i], bins=np.logspace(np.log10(min(dvxcm_vector[i])),np.log10(max(dvxcm_vector[i])),16), weights = weights,  histtype='stepfilled', facecolor='None', edgecolor=rgba, linewidth=2.)
    plt.xlabel(r'$\Delta v_{x,cm}$', fontsize=18)
    plt.xscale('log')
    fig6.add_subplot(222)
    plt.hist(dvycm_vector[i], bins=np.logspace(np.log10(min(dvycm_vector[i])),np.log10(max(dvycm_vector[i])),16), weights = weights,  histtype='stepfilled', facecolor='None',edgecolor=rgba, linewidth=2.)
    plt.xlabel(r'$\Delta v_{y,cm}$', fontsize=18)
    plt.xscale('log')
    fig6.add_subplot(223)
    plt.hist(dvzcm_vector[i], bins=np.logspace(np.log10(min(dvzcm_vector[i])),np.log10(max(dvzcm_vector[i])),16), weights = weights,  histtype='stepfilled', facecolor='None', edgecolor=rgba, linewidth=2.)
    plt.xlabel(r'$\Delta v_{z,cm}$', fontsize=18)
    plt.xscale('log')
    fig6.add_subplot(224)
    plt.hist(dvcm_vector[i], bins=np.logspace(np.log10(min(dvcm_vector[i])),np.log10(max(dvcm_vector[i])),16), weights = weights,  histtype='stepfilled', facecolor='None', label=labelsv[i], edgecolor=rgba, linewidth=2.)
    plt.xlabel(r'$\Delta v_{cm}$', fontsize=18)
    plt.xscale('log') 
    plt.legend(fontsize=18)  


"""
fig6.legend(labelsv[::2],     # The line objects
           loc="center right",   # Position of legend
           borderaxespad=0.1,    # Small spacing around legend box
           fontsize=16
           )       
plt.legend()
"""
plt.tight_layout()
fig6.savefig('vcm_histograms.png')


dvxcm_comp = []
dvycm_comp = []
dvzcm_comp = []
dvcm_comp = []



#Kolgoromov-Smirnov test for the velocity c.o.m. distances

for i in range(len(dvxcm_vector)):
    for j in range(i+1,len(dvxcm_vector)):
        dvxcm_comp.append(ks_2samp(dvxcm_vector[i],dvxcm_vector[j])[1])
        dvycm_comp.append(ks_2samp(dvycm_vector[i],dvycm_vector[j])[1])
        dvzcm_comp.append(ks_2samp(dvzcm_vector[i],dvzcm_vector[j])[1])
        dvcm_comp.append(ks_2samp(dvcm_vector[i],dvcm_vector[j])[1])


fig8= plt.figure(figsize=(10,10))

fig8.add_subplot(221)
plt.hist(dvxcm_comp, bins=np.logspace(np.log10(min(dvxcm_comp)),np.log10(max(dvxcm_comp)),40))
plt.xlabel('pvalue - vxcm')
plt.xscale('log')
fig8.add_subplot(222)
plt.hist(dvycm_comp, bins=np.logspace(np.log10(min(dvycm_comp)),np.log10(max(dvycm_comp)),40))
plt.xlabel('pvalue - vycm')
plt.xscale('log')
fig8.add_subplot(223)
plt.hist(dvzcm_comp, bins=np.logspace(np.log10(min(dvzcm_comp)),np.log10(max(dvzcm_comp)),40))
plt.xlabel('pvalue - vzcm')
plt.xscale('log')
fig8.add_subplot(224)
plt.hist(dvcm_comp, bins=np.logspace(np.log10(min(dvcm_comp)),np.log10(max(dvcm_comp)),40))
plt.xlabel('pvalue - vcm')
plt.xscale('log') 

plt.savefig("ks_test_vcm.png")
#plt.show()
plt.close()  

#Plot the distributions of c.o.m. distances for the cosines of the angle


fig20= plt.figure(figsize=(12,8))

for i in range(0,len(theta_vector),2):

    weights= np.zeros(len(theta_vector[i]))+1./float(len(theta_vector[i]))

    rgba = cmap(np.log10(0.5+1.25*i))

    plt.hist(theta_vector[i], bins=np.linspace(min(theta_vector[i]), max(theta_vector[i]),16), weights = weights,  histtype='stepfilled', facecolor='None', label=labelsv[i], edgecolor=rgba, linewidth=2.)
    plt.xlabel(r'$\theta_{d_{cm}-dv_{cm}}$')
    #plt.xscale('log')
    plt.legend(fontsize=18, loc='upper left')

"""   
fig20.legend(labelsv,     # The line objects
           loc="center right",   # Position of legend
           borderaxespad=0.1,    # Small spacing around legend box
           )       
plt.legend()
"""
fig20.savefig('theta_histograms.png')


#####################################################################################################################################

#####################################################################################################################################


#Plot the differences of the mass ratios, the distances of the c.o.m. and the velocity distances of the c.o.m. as a function of the splitting number


fig9= plt.figure(figsize=(12,10))
sim_v = [0,4,8]
count = 1

for i in sim_v:
    fig9.add_subplot(3,len(sim_v)+1,count)
    appo = np.linspace(1,len(nodes_plot_vector[i]),len(nodes_plot_vector[i]))
    plt.title(labelsv[i],fontsize=16)
    plt.plot(appo, nodes_plot_vector[i][::-1], lw=0.00001, alpha=0.5, marker=".")
    #plt.xlabel(r'$N_{splitting}$')
    if(count==1):
        plt.ylabel(r'$\Delta m_{splitting}$',fontsize=16) 
    plt.xscale('log')
    plt.yscale('log')
    plt.ylim(0.0002,1.1)

    fig9.add_subplot(3,len(sim_v)+1,len(sim_v)+1)
    appo1 = np.linspace(0,1,len(nodes_plot_vector[i]))
    plt.title("All")
    plt.plot(appo1, nodes_plot_vector[i][::-1], lw=0.00001, alpha=0.2, marker=".", label=labelsv[i])
    #plt.xlabel(r'$N_{splitting}$')
    if(i==0):
        plt.ylabel(r'$\Delta m_{splitting}$',fontsize=16) 
    plt.xscale('log')
    plt.yscale('log')
    plt.ylim(0.0002,1.1)
    plt.legend(loc=3)

    fig9.add_subplot(3,len(sim_v)+1,len(sim_v)+1+count)
    plt.plot(appo, dcm_vector[i][::-1], lw=0.00001, alpha=0.5, marker=".")
    #plt.xlabel(r'$N_{splitting}$')
    if(count==1):
        plt.ylabel(r'$d_{cm,splitting}$',fontsize=16)
    plt.yscale('log')
    plt.xscale('log')
    plt.ylim(0.0001,40)

    fig9.add_subplot(3,len(sim_v)+1,2*(len(sim_v)+1))
    plt.plot(appo1, dcm_vector[i][::-1]/max(dcm_vector[i][::-1]), lw=0.00001, alpha=0.2, marker=".", label=labelsv[i])
    #plt.xlabel(r'$N_{splitting}$')
    if(i==0):
        plt.ylabel(r'$d_{cm,splitting}$',fontsize=16)
    plt.yscale('log')
    plt.xscale('log')
    plt.ylim(0.00001,1.1)
    plt.legend(loc=3)  

    fig9.add_subplot(3,len(sim_v)+1,2*(len(sim_v)+1)+count)
    plt.plot(appo, dvcm_vector[i][::-1], lw=0.00001, alpha=0.5, marker=".")
    plt.xlabel(r'$N_{splitting}$',fontsize=16)
    plt.xscale('log')
    plt.yscale('log')
    if(count==1):
        plt.ylabel(r'$\Delta v_{cm,splitting}$',fontsize=16)
    plt.ylim(0.004,20) 

    fig9.add_subplot(3,len(sim_v)+1,3*(len(sim_v)+1))
    plt.plot(appo1, dvcm_vector[i][::-1]/max(dvcm_vector[i][::-1]), lw=0.00001, alpha=0.2, marker=".", label=labelsv[i])
    plt.xlabel(r'$f_{splitting}$',fontsize=16)
    plt.xscale('log')
    plt.yscale('log')
    if(i==0):
        plt.ylabel(r'$\Delta v_{cm,splitting}$',fontsize=16)
    plt.ylim(0.0008,1.1)
    plt.legend(loc=3)     

    count+=1    
        
plt.tight_layout()     
plt.savefig("parameters.png") 


#####################################################################################################################################

#####################################################################################################################################


#Plot the differences of the mass ratios, the distances of the c.o.m. and the velocity distances of the c.o.m. as a function each other
#This is done to understand if there are some correlations

#1. The weight of the plot is the third quantity

fig10= plt.figure(figsize=(16,10))
count =1

for i in sim_v:

    plot_matrix = np.column_stack((nodes_plot_vector[i], dcm_vector[i], dvcm_vector[i]))
    
    #print(plot_matrix)

    fig10.add_subplot(3,len(sim_v),count)
    plot_matrix = np.array(sorted(plot_matrix,key=lambda x: x[0]))
    plt.title(labelsv[i])
    plt.scatter(plot_matrix[:,0], plot_matrix[:,1], c=plot_matrix[:,2], norm=colors.LogNorm(), marker=".")
    plt.xlabel(r'$\Delta m_{splitting}$', fontsize=16)
    if(i==sim_v[0]):
        plt.ylabel(r'$d_{cm,splitting}$', fontsize=16)
    plt.xscale('log')
    plt.yscale('log')    
    plt.xlim(0.0002,1.1)
    plt.ylim(0.0001,40)
    plt.clim(0.004,20)
    if(i==sim_v[-1]):
        plt.colorbar().ax.set_ylabel(r'$\Delta v_{cm}$', rotation=270, fontsize=16,labelpad=15)


    fig10.add_subplot(3,len(sim_v),len(sim_v)+count)
    plot_matrix = np.array(sorted(plot_matrix,key=lambda x: x[0]))
    #print(plot_matrix)
    plt.scatter(plot_matrix[:,0], plot_matrix[:,2], c=plot_matrix[:,1], norm=colors.LogNorm(), marker=".")
    plt.xscale('log')
    plt.yscale('log')
    plt.xlabel(r'$\Delta m_{splitting}$', fontsize=16)
    if(i==sim_v[0]):
        plt.ylabel(r'$\Delta v_{cm,splitting}$', fontsize=16)
    plt.xlim(0.0002,1.1)
    plt.ylim(0.004,20)
    plt.clim(0.0001,40)
    if(i==sim_v[-1]):
        plt.colorbar().ax.set_ylabel(r'$d_{cm}$', rotation=270, fontsize=16,labelpad=15)
    
    fig10.add_subplot(3,len(sim_v),2*len(sim_v)+count)
    plot_matrix = np.array(sorted(plot_matrix,key=lambda x: x[1]))
    plt.scatter(plot_matrix[:,1], plot_matrix[:,2], c=plot_matrix[:,0], norm=colors.LogNorm(), marker=".")
    plt.plot(np.arange(0.0001, 40), 1.0*(np.arange(0.0001, 40)**(1.)), label=r"$r$")
    plt.plot(np.arange(0.0001, 40), 0.01*(np.arange(0.0001, 40)**(-0.5)), label=r"$r^{-1/2}$")
      
    plt.xlabel(r'$d_{cm,splitting}$', fontsize=16)
    if(i==sim_v[0]):
        plt.ylabel(r'$\Delta v_{cm,splitting}$', fontsize=16)
    plt.xscale('log')
    plt.yscale('log')
    plt.xlim(0.0001,40)
    plt.ylim(0.004,20) 
    plt.legend()
    plt.clim(10**-2,1)
    if(i==sim_v[-1]):
        plt.colorbar().ax.set_ylabel(r'$\Delta m$', rotation=270, fontsize=16, labelpad=15)
    plt.tight_layout()
    count+=1
       

plt.tight_layout()
plt.savefig("dependences.png")


#####################################################################################################################################

#####################################################################################################################################


#Plot the differences of the mass ratios, the distances of the c.o.m. and the velocity distances of the c.o.m. as a function of the splitting number


#2. The weight of the plots is the progressive number of the splitting

fig11= plt.figure(figsize=(16,10))
count =1

for i in sim_v:

    plot_matrix = np.column_stack((nodes_plot_vector[i], dcm_vector[i], dvcm_vector[i]))
    appo = np.linspace(1,len(plot_matrix[:,1]),len(plot_matrix[:,1]))
    appo=appo[::-1]
    #print(plot_matrix)

    fig11.add_subplot(3,len(sim_v),count)
    #plot_matrix = np.array(sorted(plot_matrix,key=lambda x: x[0]))
    plt.title(labelsv[i])
    plt.scatter(plot_matrix[:,0], plot_matrix[:,1], c=appo, norm=colors.LogNorm(), marker=".")
    plt.xlabel(r'$\Delta m_{splitting}$')
    plt.ylabel(r'$d_{cm,splitting}$')
    plt.xscale('log')
    plt.yscale('log')    
    plt.xlim(0.0002,1.1)
    plt.ylim(0.0001,40)
    plt.clim(1,len(plot_matrix[:,1]))
    plt.colorbar().ax.set_ylabel(r'$N_{splitting}$', rotation=270)


    fig11.add_subplot(3,len(sim_v),len(sim_v)+count)
    #plot_matrix = np.array(sorted(plot_matrix,key=lambda x: x[0]))
    #print(plot_matrix)
    plt.scatter(plot_matrix[:,0], plot_matrix[:,2], c=appo, norm=colors.LogNorm(), marker=".")
    plt.xscale('log')
    plt.yscale('log')
    plt.xlabel(r'$\Delta m_{splitting}$')
    plt.ylabel(r'$\Delta v_{cm,splitting}$')
    plt.xlim(0.0002,1.1)
    plt.ylim(0.004,20)
    plt.clim(1,len(plot_matrix[:,1]))
    plt.colorbar().ax.set_ylabel(r'$N_{splitting}$', rotation=270)
 
    fig11.add_subplot(3,len(sim_v),2*len(sim_v)+count)
    #plot_matrix = np.array(sorted(plot_matrix,key=lambda x: x[1]))
    plt.scatter(plot_matrix[:,1], plot_matrix[:,2], c=appo, norm=colors.LogNorm(), marker=".")
    plt.plot(np.arange(0.0001, 40), 1.0*(np.arange(0.0001, 40)**(1.)), label=r"$r$")
    plt.plot(np.arange(0.0001, 40), 0.01*(np.arange(0.0001, 40)**(-0.5)), label=r"$r^{-1/2}$")
    plt.xlabel(r'$d_{cm,splitting}$')
    plt.ylabel(r'$\Delta v_{cm,splitting}$')
    plt.xscale('log')
    plt.yscale('log')
    plt.xlim(0.0001,40)
    plt.ylim(0.004,20) 
    plt.legend()
    plt.clim(1,len(plot_matrix[:,1]))
    
    plt.colorbar().ax.set_ylabel(r'$N_{splitting}$', rotation=270)
    plt.tight_layout()
    count+=1
       

plt.tight_layout()

plt.savefig("dependences_2.png")

#####################################################################################################################################

#####################################################################################################################################


#Plot each quantity as a function of the same quantity in the previous splitting. 
#This is done to understand if there is some trend in the splitting procedure


for i in sim_v:

    fig12= plt.figure(figsize=(14,10))


    #plot_matrix = np.column_stack((nodes_plot_vector[i], dcm_vector[i], dvcm_vector[i]))
    appo = np.linspace(1,len(nodes_plot_vector[i])-1,len(nodes_plot_vector[i])-1)
    appo=appo[::-1]
    #appo=appo[1:]
    #print(plot_matrix)
    dm_vector_i = nodes_plot_vector[i][:-1]
    dm_vector_iplus1 = nodes_plot_vector[i][1:]

    #print(len(dm_vector_i))

    dcm_vector_i = dcm_vector[i][:-1]
    dcm_vector_iplus1 = dcm_vector[i][1:]

    dvcm_vector_i = dvcm_vector[i][:-1]
    dvcm_vector_iplus1 = dvcm_vector[i][1:]

    theta_vector_i = theta_vector[i][:-1]
    theta_vector_iplus1 = theta_vector[i][1:]


    fig12.add_subplot(2,2,1)
    #plot_matrix = np.array(sorted(plot_matrix,key=lambda x: x[0]))
    #print(plot_matrix)
    plt.scatter(dm_vector_i, dm_vector_iplus1,c=appo, norm=colors.LogNorm(), marker=".")
    plt.xlabel(r'$dm_{cm,i}$')
    plt.ylabel(r'$dm_{cm,i+1}$')
    plt.xscale('log')
    plt.yscale('log')    
    plt.xlim(0.0002,1.1)
    plt.ylim(0.0002,1.1)
    plt.clim(1,len(appo))
    plt.colorbar().ax.set_ylabel(r'$N_{splitting}$', rotation=270)

    
    fig12.add_subplot(2,2,2)
    #plot_matrix = np.array(sorted(plot_matrix,key=lambda x: x[0]))
    plt.scatter(dcm_vector_i, dcm_vector_iplus1,c=appo, norm=colors.LogNorm(), marker=".")
    plt.xlabel(r'$d_{cm,i}$')
    plt.ylabel(r'$d_{cm,i+1}$')
    plt.xscale('log')
    plt.yscale('log')    
    plt.xlim(0.0001,40)
    plt.ylim(0.0001,40)
    plt.clim(1,len(appo))
    plt.plot(np.arange(0.0001, 40), 1.0*(np.arange(0.0001, 40)**(1.)))
    plt.colorbar().ax.set_ylabel(r'$N_{splitting}$', rotation=270)


    fig12.add_subplot(2,2,3)
    #plot_matrix = np.array(sorted(plot_matrix,key=lambda x: x[0]))
    #print(plot_matrix)
    plt.scatter(dvcm_vector_i, dvcm_vector_iplus1,c=appo, norm=colors.LogNorm(), marker=".")
    plt.xlabel(r'$dv_{cm,i}$')
    plt.ylabel(r'$dv_{cm,i+1}$')
    plt.xscale('log')
    plt.yscale('log')    
    plt.xlim(0.004,20) 
    plt.ylim(0.004,20) 
    
    plt.clim(1,len(appo))
    plt.plot(np.arange(0.0001, 40), 1.0*(np.arange(0.0001, 40)**(1.)))
    
    plt.colorbar().ax.set_ylabel(r'$N_{splitting}$', rotation=270)

    fig12.add_subplot(2,2,4)
    #plot_matrix = np.array(sorted(plot_matrix,key=lambda x: x[0]))
    #print(plot_matrix)
    plt.scatter(theta_vector_i, theta_vector_iplus1,c=appo, norm=colors.LogNorm(), marker=".")
    plt.xlabel(r'$d\theta_{cm,i}$')
    plt.ylabel(r'$d\theta_{cm,i+1}$')
    #plt.xscale('log')
    #plt.yscale('log')    
    plt.xlim(min(theta_vector_i),max(theta_vector_i)) 
    plt.ylim(min(theta_vector_iplus1),max(theta_vector_iplus1)) 
    
    #plt.ylim(0.08,100) 
    #plt.clim(1,len(plot_matrix[:,1]))
    
    plt.clim(1,len(appo))
    plt.colorbar().ax.set_ylabel(r'$N_{splitting}$', rotation=270)
    #count+=1
    plt.tight_layout()

    plt.savefig("i_iplus1"+str(i)+".png")







#####################################################################################################################################

#####################################################################################################################################

#Plot the scales

fig24= plt.figure(figsize=(8,8))


plt.scatter(scales_m, scales_r, c=scales_v,  marker=".")
plt.xlabel(r'$M_{tot}$')
plt.ylabel(r'$r_{scale}$')
plt.xscale('log')
#plt.yscale('log')    
plt.xlim(min(scales_m)/1.1,max(scales_m)*1.1)
plt.ylim(min(scales_r)/1.1,max(scales_r)*1.1)
plt.clim(min(scales_v)/1.01,max(scales_v)*1.01)
plt.colorbar().ax.set_ylabel(r'$v_{scale}$', rotation=270)



plt.tight_layout()
plt.savefig("scales.png")
