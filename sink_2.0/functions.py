import numpy as np
import itertools
import os
#import yt

G=0.00430145 #gravitational constant in astronomical units [pc]*[MSun]**(-1)*[km/s]**2 (the same as Nbody6++GPU)

PROJECT_DIR = "."


# Where to draw the sink particles
#Through yt
time_hydro = 3.
path_sink_yt = '/tank0/ballone/hydro_set_mcs_orig'

#Through genfromtxt
SINK_ID = "Sink"
SINK_PATH = os.path.join(PROJECT_DIR, SINK_ID)

# Where to save the figures
FIG_ID = "Plot"
IMAGES_PATH = os.path.join(PROJECT_DIR, FIG_ID)

# Where to save the outputs
OUT_ID = "Output"
OUTPUT_PATH = os.path.join(PROJECT_DIR, OUT_ID)
os.makedirs(OUTPUT_PATH, exist_ok=True)




#########################################################################################àà
#Input and output functions


def walklevel(some_dir, level=1):
    some_dir = some_dir.rstrip(os.path.sep)
    assert os.path.isdir(some_dir)
    num_sep = some_dir.count(os.path.sep)
    for root, dirs, files in os.walk(some_dir):
        yield root, dirs, files
        num_sep_this = root.count(os.path.sep)
        if num_sep + level <= num_sep_this:
            del dirs[:]



def search_and_sort(path=path_sink_yt, time=time_hydro):
    
    time_string = str(int(time*100))

    
    file_name = []
    file_mass = []

    # r=root, d=directories, f = files
    for r, d, f in os.walk(path):
        for file in f:
            if 'out.00'+time_string in file and '.den' not in file: 
                    file_name.append(os.path.join(r, file))
                    base = float(os.path.join(r, file)[35])
                    exp = float(os.path.join(r, file)[38])
                    file_mass.append(base*10**(exp))

    files_array = np.column_stack((file_mass, file_name))
    files_array = sorted(files_array, key = lambda x: float(x[0])) 

    files = np.empty(len(file_name),dtype='<U72')

    for i in range(len(file_name)):
        files[i] = files_array[i][1]
        print(files[i])
        
    return files

def find_sink(path=SINK_PATH, ref_word="sink", base_pos=6, exponent_pos=9):
      
    file_name = []
    file_mass = []

    # r=root, d=directories, f = files
    
    for r, d, f in walklevel(path,0):
        for file in f:
            if ref_word in file: 
                    file_name.append(os.path.join(r, file))
                    base = float(file[base_pos])
                    exp = float(file[exponent_pos])
                    file_mass.append(base*10**(exp))

    files_array = np.column_stack((file_mass, file_name))
    files_array = sorted(files_array, key = lambda x: float(x[0])) 

    files = np.empty(len(file_name),dtype='<U72')

    print("Files:")
    print("[", end = " ")
    for i in range(len(file_name)):
        files[i] = files_array[i][1]
        print(files[i], ",", end=" ")
       
    print("]")
    return files



def load_yt(simname, remove_small_masses=True, mass_limit=0.1, verbose=True):
    conv=2.22249e2
    
    ds=yt.load(simname)
    dd=ds.all_data()

    #convert the mass from n-body units to solar
    m=dd[('Stars',"Mass")]*conv
    x=dd[('Stars',"particle_position_x")]
    y=dd[('Stars',"particle_position_y")]
    z=dd[('Stars',"particle_position_z")]
    vx=dd[('Stars',"particle_velocity_x")]
    vy=dd[('Stars',"particle_velocity_y")]
    vz=dd[('Stars',"particle_velocity_z")]
    
    #print(len(m[m[:]<0.1]),len(m[m[:]>=0.1]))
    
    if(remove_small_masses==True):
        if(verbose==True):
            print("Number of sinks with mass below lower limit ("+str(mass_limit)+" MSun):", len(m[m[:]<0.1]))
        #I decided to simply remove the sinks that are less massive than 0.1 solar masses, if there is any (since it is difficult to treat them and they have a ridicolous total mass)
        x=x[m[:]>=0.1]
        y=y[m[:]>=0.1]
        z=z[m[:]>=0.1]
        vx=vx[m[:]>=0.1]
        vy=vy[m[:]>=0.1]
        vz=vz[m[:]>=0.1]
        m=m[m[:]>=0.1]
    
    if(verbose==True):    
        print("Number of sink particles:", len(m))
    
    return x, y, z, vx, vy, vz, m



def load_dat(simname, verbose=True, remove_small_masses=True, mass_limit=0.1, type_d="float", skipheader=0, skipfooter=0):
    
    if(verbose==True):
        print("Simulation under consideration:", simname)
    
    m, x, y, z, vx, vy, vz= np.genfromtxt(simname, dtype=type_d, skip_header=skipheader, skip_footer=skipfooter, comments="#", unpack=True)
    
    if(remove_small_masses==True):
        if(verbose==True):
            print("Number of sinks with mass below lower limit ("+str(mass_limit)+" MSun):", len(m[m[:]<0.1]))
        #I decided to simply remove the sinks that are less massive than 0.1 solar masses, if there is any (since it is difficult to treat them and they have a ridicolous total mass)
        x=x[m[:]>=0.1]
        y=y[m[:]>=0.1]
        z=z[m[:]>=0.1]
        vx=vx[m[:]>=0.1]
        vy=vy[m[:]>=0.1]
        vz=vz[m[:]>=0.1]
        m=m[m[:]>=0.1]
    if(verbose==True):
        print("Number of sink particles:", len(m))
    
    return m, x, y, z, vx, vy, vz


def save_fig(fig_id, tight_layout=True, fig_extension="pdf", resolution=300):
    path = os.path.join(IMAGES_PATH, fig_id + "." + fig_extension)
    print("Saving figure", fig_id)
    if tight_layout:
        plt.tight_layout()
    plt.savefig(path, format=fig_extension, dpi=resolution)