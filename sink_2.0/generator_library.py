from sklearn.cluster import AgglomerativeClustering, ward_tree
import numpy as np
import scipy.spatial as spat
from scipy.spatial import KDTree

import classes as cl
from functions import *

G=0.00430145 #gravitational constant in astronomical units [pc]*[MSun]**(-1)*[km/s]**2 (the same as Nbody6++GPU)

class tree_clustering:
    
    def __init__(self, sc, normalize=True):
        if(normalize==True):
            self.X = np.column_stack((sc.x/sc.d_var, sc.y/sc.d_var, sc.z/sc.d_var,
                                      sc.vx/sc.dv_var, sc.vy/ sc.dv_var, sc.vz/sc.dv_var))   
        else:
            self.X = np.column_stack((sc.x, sc.y, sc.z,
                                      sc.vx, scp.vy, sc.vz))
            
        self.m = sc.m
     
    
    def perform_clustering(self, n_clusters=1, linkage="ward"):
        self.n_cl = n_clusters
        self.clustering = AgglomerativeClustering(n_clusters=self.n_cl, linkage=linkage).fit(self.X)   
        
    
    def mass_clustering(self):

        leaves_mass = np.zeros(self.n_cl-1)

        for i in range(len(leaves_mass)):
            for j in range(2):
                if(self.clustering.children_[i][j]<self.X.shape[0]):
                    leaves_mass[i]+=self.m[self.clustering.children_[i][j]]
                else:
                    leaves_mass[i]+=leaves_mass[self.clustering.children_[i][j]-self.X.shape[0]]


        self.mcm = np.zeros([self.X.shape[0]-1,2])
        self.nodes_ratios = np.zeros([self.X.shape[0]-1,2])

        for i in range(self.X.shape[0]-1):
            for j in range(2):
                if(self.clustering.children_[i][j]<self.X.shape[0]):
                    self.mcm[i][j]+=self.m[self.clustering.children_[i][j]]
                    self.nodes_ratios[i][j]+=self.m[self.clustering.children_[i][j]]/leaves_mass[i]

                else:
                    self.mcm[i][j]+=leaves_mass[self.clustering.children_[i][j]-self.X.shape[0]]
                    self.nodes_ratios[i][j]+=leaves_mass[self.clustering.children_[i][j]-self.X.shape[0]]/leaves_mass[i]


        self.q = np.zeros(len(self.nodes_ratios))

        for i in range(len(self.nodes_ratios)):
            self.q[i] = self.nodes_ratios[i][0]

            
    def phase_space_clustering(self):
        xcm = np.zeros([self.X.shape[0]-1,2])
        ycm = np.zeros([self.X.shape[0]-1,2])
        zcm = np.zeros([self.X.shape[0]-1,2])

        vxcm = np.zeros([self.X.shape[0]-1,2])
        vycm = np.zeros([self.X.shape[0]-1,2])
        vzcm = np.zeros([self.X.shape[0]-1,2])

        for i in range(self.X.shape[0]-1):
            for j in range(2):
                if(self.clustering.children_[i][j]<self.X.shape[0]):

                    xcm[i][j]+=self.X[self.clustering.children_[i][j]][0]
                    ycm[i][j]+=self.X[self.clustering.children_[i][j]][1]
                    zcm[i][j]+=self.X[self.clustering.children_[i][j]][2]

                    vxcm[i][j]+=self.X[self.clustering.children_[i][j]][3]
                    vycm[i][j]+=self.X[self.clustering.children_[i][j]][4]
                    vzcm[i][j]+=self.X[self.clustering.children_[i][j]][5]


                else:	
                    appo_index = self.clustering.children_[i][j]-self.X.shape[0]
                    xcm[i][j]+=np.sum(self.mcm[appo_index]*xcm[appo_index])/np.sum(self.mcm[appo_index])
                    ycm[i][j]+=np.sum(self.mcm[appo_index]*ycm[appo_index])/np.sum(self.mcm[appo_index])
                    zcm[i][j]+=np.sum(self.mcm[appo_index]*zcm[appo_index])/np.sum(self.mcm[appo_index])

                    vxcm[i][j]+=np.sum(self.mcm[appo_index]*vxcm[appo_index])/np.sum(self.mcm[appo_index])
                    vycm[i][j]+=np.sum(self.mcm[appo_index]*vycm[appo_index])/np.sum(self.mcm[appo_index])
                    vzcm[i][j]+=np.sum(self.mcm[appo_index]*vzcm[appo_index])/np.sum(self.mcm[appo_index])

        self.dxcm = np.zeros(self.X.shape[0]-1)
        self.dycm = np.zeros(self.X.shape[0]-1)
        self.dzcm = np.zeros(self.X.shape[0]-1)	

        self.dvxcm = np.zeros(self.X.shape[0]-1)
        self.dvycm = np.zeros(self.X.shape[0]-1)
        self.dvzcm = np.zeros(self.X.shape[0]-1)	


        for i in range(self.X.shape[0]-1):
            self.dxcm[i]+=xcm[i][0]-xcm[i][1]
            self.dycm[i]+=ycm[i][0]-ycm[i][1]
            self.dzcm[i]+=zcm[i][0]-zcm[i][1]

            self.dvxcm[i]+=vxcm[i][0]-vxcm[i][1]
            self.dvycm[i]+=vycm[i][0]-vycm[i][1]
            self.dvzcm[i]+=vzcm[i][0]-vzcm[i][1]


    def theta_clustering(self):

        self.theta = np.zeros(len(self.dxcm))

        dcm = np.sqrt(self.dxcm**2.+self.dycm**2.+self.dzcm**2.)
        dvcm = np.sqrt(self.dvxcm**2.+self.dvycm**2.+self.dvzcm**2.)

        for i in range(len(self.dvycm)):
            self.theta[i] = np.arccos((self.dxcm[i]*self.dvxcm[i] + self.dycm[i]*self.dvycm[i] + self.dzcm[i]*self.dvzcm[i]) / (dcm[i]*dvcm[i]))         
    
#######################################################################################################################################################
    
class new_tree:

    def __init__(self, N_sim, changes, scales, same_sim=True, mass_change=True):
        self.N_sim = N_sim
        self.changes = changes
        self.scales = scales
        
        self.same_sim = same_sim
        self.mass_change = mass_change

        self.tree_mass = []
        self.tree_children = []
        self.tree_phase_space = [] 
               
        
    def generate_tree(self, phase_space, mass, children):
    
        n_sample = len(mass)
    
        for k in range(len(mass[self.N_sim])):

            if(k<self.changes):

                Nsim_rand = np.random.randint(n_sample)

                if(self.same_sim==False):
                    #Avoid sampling from the original simulation                       
                    while(Nsim_rand==self.N_sim):
                        Nsim_rand = np.random.randint(n_sample)

                print("Node", k,"from sim:" , Nsim_rand)

                self.generate_branch(k, phase_space, mass, children, Nsim_rand)

            else:
                self.generate_branch(k, phase_space, mass, children, self.N_sim)
            
        self.tree_mass = np.array(self.tree_mass)
        self.tree_phase_space = np.array(self.tree_phase_space)
        self.tree_children = np.array(self.tree_children)
        #return np.array(new_ps), np.array(new_m), np.array(new_ch) 
        
    
    
    def generate_branch(self, k, phase_space, mass, children, n_rand):
        
        self.tree_phase_space.append(phase_space[n_rand][::-1][k])

        if(self.mass_change==True):
            self.tree_mass.append(mass[n_rand][::-1][k]) 
        else:
            self.tree_mass.append(mass[self.N_sim][::-1][k]) 

        self.tree_children.append(children[self.N_sim][::-1][k])

    
    
    def splitting(self, phase_space_scales=False):
     
        self.split_mass = np.zeros((len(self.tree_mass),2))
        self.split_phase_space = np.zeros((len(self.tree_phase_space),6,2))


        for j in range(len(self.tree_mass)):

            if(j==0):
                #First splitting
                #Multiply for the mass scale to return to physical units

                mass_of_branch = self.scales[0]

            else:

                ind_to_split = np.where(self.tree_children-len(self.tree_children)==len(self.tree_children)-j)
                ind0= ind_to_split[0][0]
                ind1= ind_to_split[1][0]

                mass_of_branch = self.split_mass[ind0,ind1]

            self.split_mass[j,0]= self.tree_mass[j]*mass_of_branch #(1.-new_mass_vector[j])*mass_of_branch/2.
            self.split_mass[j,1]= (1.- self.tree_mass[j])*mass_of_branch #(new_mass_vector[j]+1.)*mass_of_branch/2.

            #Separate the new positions and velocities from that of the split mass

            for ps in range(6):
                if(j==0):
                    self.split_phase_space[j,ps,0] = (0. + self.tree_phase_space[j,ps]*(self.split_mass[j,1])/mass_of_branch)
                    self.split_phase_space[j,ps,1] = (0. - self.tree_phase_space[j,ps]*(self.split_mass[j,0])/mass_of_branch)

                else:
                    self.split_phase_space[j,ps,0] = (self.split_phase_space[ind0,ps,ind1] + self.tree_phase_space[j,ps]*(self.split_mass[j,1])/mass_of_branch)
                    self.split_phase_space[j,ps,1] = (self.split_phase_space[ind0,ps,ind1] - self.tree_phase_space[j,ps]*(self.split_mass[j,0])/mass_of_branch)


        self.leaves_indexes = self.tree_children[:,:]-len(self.tree_children)<1

        if(phase_space_scales==True):
            for i in range(3):
                self.split_phase_space[:,i,:] = self.split_phase_space[:,i,:]*self.scales[1]
                self.split_phase_space[:,i+3,:] = self.split_phase_space[:,i,:]*self.scales[2]        


    def leaf_fall(self, cut_min=False, minimum_m=0):
     
        x = self.split_phase_space[:,0,:][self.leaves_indexes]
        y = self.split_phase_space[:,1,:][self.leaves_indexes]
        z = self.split_phase_space[:,2,:][self.leaves_indexes]

        vx = self.split_phase_space[:,3,:][self.leaves_indexes]
        vy = self.split_phase_space[:,4,:][self.leaves_indexes]
        vz = self.split_phase_space[:,5,:][self.leaves_indexes]

        m = self.split_mass[self.leaves_indexes]
        
        self.leaves = cl.cluster(m, x, y, z, vx, vy, vz)

        if(cut_min==True):
            self.pruning(minimum_m)   

        std_pos = np.sqrt((np.var(self.leaves.x)+np.var(self.leaves.y)+np.var(self.leaves.z))/3.)
        std_vel = np.sqrt((np.var(self.leaves.vx)+np.var(self.leaves.vy)+np.var(self.leaves.vz))/3.)

        print("Std leaves pos:", std_pos)
        print("Std leaves vel:", std_vel)

        self.leaves.x = self.leaves.x * self.scales[1] / std_pos
        self.leaves.y = self.leaves.y * self.scales[1] / std_pos
        self.leaves.z = self.leaves.z * self.scales[1] / std_pos

        self.leaves.vx = self.leaves.vx * self.scales[2] / std_vel
        self.leaves.vy = self.leaves.vy * self.scales[2] / std_vel
        self.leaves.vz = self.leaves.vz * self.scales[2] / std_vel

  
    def pruning(self, min_mass, tol=0.0001):
        
        self.leaves.x = self.leaves.x[self.leaves.m[:]>=min_mass-(tol*min_mass)]
        self.leaves.y = self.leaves.y[self.leaves.m[:]>=min_mass-(tol*min_mass)]
        self.leaves.z = self.leaves.z[self.leaves.m[:]>=min_mass-(tol*min_mass)]

        self.leaves.vx = self.leaves.vx[self.leaves.m[:]>=min_mass-(tol*min_mass)]
        self.leaves.vy = self.leaves.vy[self.leaves.m[:]>=min_mass-(tol*min_mass)]
        self.leaves.vz = self.leaves.vz[self.leaves.m[:]>=min_mass-(tol*min_mass)]

        self.leaves.m = self.leaves.m[self.leaves.m[:]>=min_mass-(tol*min_mass)]       
    