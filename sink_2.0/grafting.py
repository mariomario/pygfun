from tree_library import *
from classes import *
from functions import *


Nsim = 0 #Simulation to generate is: m(Nsim+1)e4
n_changes = 3 #Number of changes in the tree
N_generations = 100

cut = True #Lower mass cut in the mass spectrum

#Main 

#####################################################################################################################################

#ORIGINAL TREE

#####################################################################################################################################

#First we create the "original" tree from which we will generate the new one

sink_woods = woods()
sink_woods.load_woods()
sink_woods.plant_woods()


sink_orig = cluster(sink_woods.files[Nsim], False)
sink_orig.find_and_rescale_com(False)
sink_orig.energies()




#####################################################################################################################################

    #NEW TREE

#####################################################################################################################################


new_woods = woods()

new_woods.new_generation(N_generations, Nsim, n_changes, sink_woods, sink_orig, cut, min(sink_orig.m))