from sklearn.cluster import AgglomerativeClustering, ward_tree
import numpy as np
import itertools
import matplotlib
matplotlib.use('Agg') 
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from scipy.stats import ks_2samp
from mpl_toolkits.mplot3d import Axes3D
from scipy.cluster.hierarchy import linkage, fcluster, dendrogram
import os
import yt


G=0.00430145 #gravitational constant in astronomical units [pc]*[MSun]**(-1)*[km/s]**2 (the same as Nbody6++GPU)

#Main
# It read sinks at a time t

t=2.0

gandnum=t*10.+1.
gandlab='{:02.0f}'.format(gandnum)

gasnum=t*100.
gaslab='{:03.0f}'.format(gasnum)

#GANDALF READING

x_gand=[]
y_gand=[]
z_gand=[]
vx_gand=[]
vy_gand=[]
vz_gand=[]
m_gand=[]

#GASOLINE READING, THROUGH YT

path = '/tank0/ballone/hydro_set_mcs_orig'

files = []
files2 = []
# r=root, d=directories, f = files
for r, d, f in os.walk(path):
    for file in f:
        if 'out.00300' in file and '.den' not in file: 
                files.append(os.path.join(r, file))
                 #print(files[-1])

numbers = np.array([1,10,11,2,3,4,5,6,7,8,9])
files.sort()
files_array = np.column_stack((numbers, files))
files_array = sorted(files_array, key = lambda x: float(x[0])) 
#print(files_array)
for i in range(len(files)):
    files[i] = files_array[i][1]
#files = np.sort(files)
print(files)

conv=2.22249e2 
labelsv = []  
nodes_ratios_vector = [] 
nodes_vector_vector = []
nodes_vector1_vector = []
nodes_vector2_vector = []
nodes_vector3_vector = []
nodes_vector4_vector = []
nodes_vector5_vector = []

dxcm_vector = []
dycm_vector = []
dzcm_vector = []
dcm_vector = []

dvxcm_vector = []
dvycm_vector = []
dvzcm_vector = []
dvcm_vector = []

theta_vector = []
nodes_plot_vector = []

scales_m = []
scales_r = []
scales_v = []

np.random.seed(886)

#####################################################################################################################################

#ORIGINAL TREE

#####################################################################################################################################

#First we create the "original" tree from which we will generate the new one
ind_sim = 0
Ni = 1
files=[files[ind_sim]] #I descide to consider only the first simulation
print("\n")
print(files)

f_counter=0
colorv = ['orange','blue','green',"yellow","black",'red',"cyan","magenta","brown","pink"]
for f in files:
    #GANDALF READING

    print(f)

    x_gand=[]
    y_gand=[]
    z_gand=[]
    vx_gand=[]
    vy_gand=[]
    vz_gand=[]
    m_gand=[]

    #GASOLINE READING, THROUGH YT

    simname = f
    simnamered = str(f[34:39])
    labelsv.append(simnamered)
    print(simnamered)

    print("\n")

    ds=yt.load(f)
    dd=ds.all_data()

    #convert the mass from n-body units to solar
    m_gand=dd[('Stars',"Mass")]*conv
    x_gand=dd[('Stars',"particle_position_x")]
    y_gand=dd[('Stars',"particle_position_y")]
    z_gand=dd[('Stars',"particle_position_z")]
    vx_gand=dd[('Stars',"particle_velocity_x")]
    vy_gand=dd[('Stars',"particle_velocity_y")]
    vz_gand=dd[('Stars',"particle_velocity_z")]

    if(f_counter==0):
        m_gand0=np.array(m_gand)
        x_gand0=np.array(x_gand)
        y_gand0=np.array(y_gand)
        z_gand0=np.array(z_gand)
        vx_gand0=np.array(vx_gand)
        vy_gand0=np.array(vy_gand)
        vz_gand0=np.array(vz_gand)
        f_counter+=1

    x_cm_sink = np.sum(x_gand*m_gand) /np.sum(m_gand)
    y_cm_sink = np.sum(y_gand*m_gand) /np.sum(m_gand)
    z_cm_sink = np.sum(z_gand*m_gand) /np.sum(m_gand)
    vx_cm_sink = np.sum(vx_gand*m_gand) /np.sum(m_gand)
    vy_cm_sink = np.sum(vy_gand*m_gand) /np.sum(m_gand)
    vz_cm_sink = np.sum(vz_gand*m_gand) /np.sum(m_gand)

    print "Sink center of mass position: ", x_cm_sink, " [pc] ", y_cm_sink, " [pc] ", z_cm_sink, " [pc] "
    print "Sink center of mass velocity: ", vx_cm_sink, " [km/s] ", vy_cm_sink, " [km/s] ", vz_cm_sink, " [km/s] "
    x_gand = x_gand - x_cm_sink
    y_gand = y_gand - y_cm_sink
    z_gand = z_gand - z_cm_sink
    vx_gand = vx_gand - vx_cm_sink
    vy_gand = vy_gand - vy_cm_sink
    vz_gand = vz_gand - vz_cm_sink


    Eksinkv= []
    Egsinkv= []

    dist = []
    distv = []

    for j in range(len(m_gand)):
        Eksinkv.append(1./2.*m_gand[j]*(vx_gand[j]**2+vy_gand[j]**2+vz_gand[j]**2))
        mvectappo = np.delete(m_gand,j)
        xvectappo = np.delete(x_gand,j)
        yvectappo = np.delete(y_gand,j)
        zvectappo = np.delete(z_gand,j)
        vxvectappo = np.delete(vx_gand,j)
        vyvectappo = np.delete(vy_gand,j)
        vzvectappo = np.delete(vz_gand,j)
        
        Egsinkv.append(np.sum(-0.5*G*m_gand[j]*mvectappo /np.sqrt((x_gand[j]-xvectappo)**2+(y_gand[j]-yvectappo)**2+(z_gand[j]-zvectappo)**2)))
        dist.append(np.sqrt((x_gand[j]-xvectappo)**2+(y_gand[j]-yvectappo)**2+(z_gand[j]-zvectappo)**2))
        distv.append(np.sqrt((vx_gand[j]-vxvectappo)**2+(vy_gand[j]-vyvectappo)**2+(vz_gand[j]-vzvectappo)**2))
    
    Eksink = sum(Eksinkv)
    Egsink = abs(sum(Egsinkv))
    vratio_sink = 2.*Eksink/Egsink

    virial_radius = G*np.sum(m_gand)**2 /(2.*Egsink)
    t_scale = np.sqrt(virial_radius*virial_radius*virial_radius/(G*np.sum(m_gand)))
    v_scale = virial_radius/np.sqrt(virial_radius*virial_radius*virial_radius/(G*np.sum(m_gand)))



    m=np.array(m_gand)
    d_var = np.sqrt((np.var(x_gand)+np.var(y_gand)+np.var(z_gand))/3.)
    dv_var = np.sqrt((np.var(vx_gand)+np.var(vy_gand)+np.var(vz_gand))/3.)
    
    scales_m.append(np.sum(m_gand))
    scales_r.append(d_var)
    scales_v.append(dv_var)

    X=np.column_stack((x_gand/d_var,y_gand/d_var,z_gand/d_var,vx_gand/dv_var,vy_gand/dv_var,vz_gand/dv_var))
    V=np.column_stack((vx_gand/dv_var,vy_gand/dv_var,vz_gand/dv_var))
    i_sort=np.argsort(m_gand)
    print(np.sum(m_gand))

    #X=np.column_stack((x_gand/np.mean(dist),y_gand/np.mean(dist),z_gand/np.mean(dist)))
    #V=np.column_stack((vx_gand/np.mean(dist),vy_gand/np.mean(dist),vz_gand/np.mean(dist)))
    

    clustering = AgglomerativeClustering(n_clusters=X.shape[0],linkage="ward").fit(X) #n_clusters=X.shape[0] to make the full tree
    leaves_mass = np.zeros(X.shape[0]-1)

    #X=np.column_stack((x_gand,y_gand,z_gand))
    #V=np.column_stack((vx_gand,vy_gand,vz_gand))
    

    for i in range(X.shape[0]-1):
        for j in range(2):
            if(clustering.children_[i][j]<X.shape[0]):
                leaves_mass[i]+=m[clustering.children_[i][j]]
            else:
                leaves_mass[i]+=leaves_mass[clustering.children_[i][j]-X.shape[0]]

    #ax = fig1.add_subplot(2,5,Ni, projection='3d')
    #ax.scatter(X[:,0],X[:,1],X[:,2],c=clustering.labels_)
    #ax.set_xlabel('x [pc]')
    #ax.set_ylabel('y [pc]')
    #ax.set_zlabel('z [pc]')

   

    nodes_ratios = np.zeros([X.shape[0]-1,2])

    nodes_ratios_vector.append(nodes_ratios[:,1])
    mcm = np.zeros([X.shape[0]-1,2])

    for i in range(X.shape[0]-1):
        for j in range(2):
            if(clustering.children_[i][j]<X.shape[0]):
                nodes_ratios[i][j]+=m[clustering.children_[i][j]]/leaves_mass[i]
                mcm[i][j]+=m[clustering.children_[i][j]]
                #nodes_ratios1[i][j]+=m[clustering.children_[i][j]]/(leaves_mass[i]-m[clustering.children_[i][j]])
            else:
                nodes_ratios[i][j]+=leaves_mass[clustering.children_[i][j]-X.shape[0]]/leaves_mass[i]
                mcm[i][j]+=leaves_mass[clustering.children_[i][j]-X.shape[0]]
                #nodes_ratios1[i][j]+=leaves_mass[clustering.children_[i][j]-X.shape[0]]/(leaves_mass[i]-leaves_mass[clustering.children_[i][j]-X.shape[0]])
        #print(nodes_ratios[i])

    nodes_plot = []
    

    for i in range(len(nodes_ratios)):
        nodes_plot.append(max(nodes_ratios[i])-min(nodes_ratios[i])) 
    
    nodes_plot_vector.append(nodes_plot)
    
    nodes_vector=np.array(nodes_ratios.flatten())

    nodes_vector_vector.append(nodes_vector)

    nodes_vector1 = nodes_vector[-10:]
    nodes_vector2 = nodes_vector[-100:]
    nodes_vector3 = nodes_vector[-10:]
    nodes_vector4 = nodes_vector[-2000:]
    nodes_vector5 = nodes_vector[:2000]

    nodes_vector1_vector.append(nodes_vector1)
    nodes_vector2_vector.append(nodes_vector2)
    nodes_vector3_vector.append(nodes_vector3)
    nodes_vector4_vector.append(nodes_vector4)
    nodes_vector5_vector.append(nodes_vector5)

    xcm = np.zeros([X.shape[0]-1,2])
    ycm = np.zeros([X.shape[0]-1,2])
    zcm = np.zeros([X.shape[0]-1,2])
    for i in range(X.shape[0]-1):
        for j in range(2):
            if(clustering.children_[i][j]<X.shape[0]):
                xcm[i][j]+=X[clustering.children_[i][j]][0]
                ycm[i][j]+=X[clustering.children_[i][j]][1]
                zcm[i][j]+=X[clustering.children_[i][j]][2]
                #nodes_ratios1[i][j]+=m[clustering.children_[i][j]]/(leaves_mass[i]-m[clustering.children_[i][j]])
            else:	
                xcm[i][j]+=np.sum(mcm[clustering.children_[i][j]-X.shape[0]]*xcm[clustering.children_[i][j]-X.shape[0]])/np.sum(mcm[clustering.children_[i][j]-X.shape[0]])
                ycm[i][j]+=np.sum(mcm[clustering.children_[i][j]-X.shape[0]]*ycm[clustering.children_[i][j]-X.shape[0]])/np.sum(mcm[clustering.children_[i][j]-X.shape[0]])
                zcm[i][j]+=np.sum(mcm[clustering.children_[i][j]-X.shape[0]]*zcm[clustering.children_[i][j]-X.shape[0]])/np.sum(mcm[clustering.children_[i][j]-X.shape[0]])

    dcm = np.zeros(X.shape[0]-1)
    dxcm = np.zeros(X.shape[0]-1)
    dycm = np.zeros(X.shape[0]-1)
    dzcm = np.zeros(X.shape[0]-1)	
    dxcm1 = np.zeros(X.shape[0]-1)
    dycm1 = np.zeros(X.shape[0]-1)
    dzcm1 = np.zeros(X.shape[0]-1)	


    for i in range(X.shape[0]-1):
        dxcm[i]+=abs(xcm[i][0]-xcm[i][1])
        dycm[i]+=abs(ycm[i][0]-ycm[i][1])
        dzcm[i]+=abs(zcm[i][0]-zcm[i][1])

        dcm[i]+=np.sqrt(dxcm[i]**2+dycm[i]**2+dzcm[i]**2)

    for i in range(X.shape[0]-1):
        dxcm1[i]+=xcm[i][0]-xcm[i][1]
        dycm1[i]+=ycm[i][0]-ycm[i][1]
        dzcm1[i]+=zcm[i][0]-zcm[i][1]

 
    #dcm = dcm/dcm[X.shape[0]-2]
    #print(dcm)
    
    dxcm_vector.append(dxcm)
    dycm_vector.append(dycm)
    dzcm_vector.append(dzcm)
    dcm_vector.append(dcm)

    vxcm = np.zeros([V.shape[0]-1,2])
    vycm = np.zeros([V.shape[0]-1,2])
    vzcm = np.zeros([V.shape[0]-1,2])
    for i in range(V.shape[0]-1):
        for j in range(2):
            if(clustering.children_[i][j]<V.shape[0]):
                vxcm[i][j]+=V[clustering.children_[i][j]][0]
                vycm[i][j]+=V[clustering.children_[i][j]][1]
                vzcm[i][j]+=V[clustering.children_[i][j]][2]
                #nodes_ratios1[i][j]+=m[clustering.children_[i][j]]/(leaves_mass[i]-m[clustering.children_[i][j]])
            else:	
                vxcm[i][j]+=np.sum(mcm[clustering.children_[i][j]-V.shape[0]]*vxcm[clustering.children_[i][j]-V.shape[0]])/np.sum(mcm[clustering.children_[i][j]-V.shape[0]])
                vycm[i][j]+=np.sum(mcm[clustering.children_[i][j]-V.shape[0]]*vycm[clustering.children_[i][j]-V.shape[0]])/np.sum(mcm[clustering.children_[i][j]-V.shape[0]])
                vzcm[i][j]+=np.sum(mcm[clustering.children_[i][j]-V.shape[0]]*vzcm[clustering.children_[i][j]-V.shape[0]])/np.sum(mcm[clustering.children_[i][j]-V.shape[0]])

    dvcm = np.zeros(V.shape[0]-1)
    dvxcm = np.zeros(V.shape[0]-1)
    dvycm = np.zeros(V.shape[0]-1)
    dvzcm = np.zeros(V.shape[0]-1)

    dvxcm1 = np.zeros(V.shape[0]-1)
    dvycm1 = np.zeros(V.shape[0]-1)
    dvzcm1 = np.zeros(V.shape[0]-1)	
	

    for i in range(V.shape[0]-1):
        dvxcm[i]+=abs(vxcm[i][0]-vxcm[i][1])
        dvycm[i]+=abs(vycm[i][0]-vycm[i][1])
        dvzcm[i]+=abs(vzcm[i][0]-vzcm[i][1])

        dvcm[i]+=np.sqrt(dvxcm[i]**2+dvycm[i]**2+dvzcm[i]**2)

    for i in range(V.shape[0]-1):
        dvxcm1[i]+=vxcm[i][0]-vxcm[i][1]
        dvycm1[i]+=vycm[i][0]-vycm[i][1]
        dvzcm1[i]+=vzcm[i][0]-vzcm[i][1]

    #dvcm = dvcm/dvcm[X.shape[0]-2]
    dvxcm_vector.append(dvxcm)
    dvycm_vector.append(dvycm)
    dvzcm_vector.append(dvzcm)
    dvcm_vector.append(dvcm)

    theta = np.zeros(V.shape[0]-1)
    
    for i in range(len(dvcm)):
        theta[i] = (dxcm1[i]*dvxcm1[i] + dycm1[i]*dvycm1[i] + dzcm1[i]*dvzcm1[i]) / (dcm[i]*dvcm[i])  
        
    theta_vector.append(theta)

    Ni+=1
  


#Plot the original distributions

Nsim = 0


fig3= plt.figure(3,figsize=(12,8))
fig9= plt.figure(9,figsize=(16,8))

plt.figure(3)
weights= np.zeros(len(dcm_vector[Nsim]))+1./float(len(dcm_vector[Nsim]))

fig3.add_subplot(221)
plt.hist(nodes_plot_vector[Nsim], bins=np.logspace(np.log10(min(nodes_plot_vector[Nsim])),np.log10(max(nodes_plot_vector[Nsim])),30), weights = weights,  histtype='stepfilled', facecolor='None', edgecolor="Crimson", linewidth=2.)

fig3.add_subplot(222)
plt.hist(dcm_vector[Nsim], bins=np.logspace(np.log10(min(dcm_vector[Nsim])),np.log10(max(dcm_vector[Nsim])),30), weights = weights,  histtype='stepfilled', facecolor='None', edgecolor="Crimson", linewidth=2.)

fig3.add_subplot(223)
plt.hist(dvcm_vector[Nsim], bins=np.logspace(np.log10(min(dvcm_vector[Nsim])),np.log10(max(dvcm_vector[Nsim])),30), weights = weights,  histtype='stepfilled', facecolor='None', edgecolor="Crimson", linewidth=2.)

fig3.add_subplot(224)
plt.hist(theta_vector[Nsim], bins=np.linspace(min(theta_vector[Nsim]),max(theta_vector[Nsim]),30), weights = weights,  histtype='stepfilled', facecolor='None', edgecolor="Crimson", linewidth=2.)


appo = np.linspace(1,len(nodes_plot_vector[Nsim]),len(nodes_plot_vector[Nsim]))
appo = appo[::-1]

plt.figure(9)
fig9.add_subplot(1,4,1)
plt.plot(appo, nodes_plot_vector[Nsim], lw=0.00001, alpha=0.5, marker=".", color="Orange")

fig9.add_subplot(1,4,2)
plt.plot(appo, dcm_vector[Nsim], lw=0.00001, alpha=0.5, marker=".", color="Orange")

fig9.add_subplot(1,4,3)
plt.plot(appo, dvcm_vector[Nsim], lw=0.00001, alpha=0.5, marker=".", color="Orange")

fig9.add_subplot(1,4,4)
plt.plot(appo, theta_vector[Nsim], lw=0.00001, alpha=0.5, marker=".", color="Orange")

plt.tight_layout()     


#####################################################################################################################################

#NEW TREE

#####################################################################################################################################

#First we take the existing tree. We consider a bubble around each level and we sample from this bubble a new splitting.
#We study the dependence of the new splitting on the width of the bubble

new_mass_vector = np.zeros(len(dcm_vector[Nsim]))
new_dcm_vector = np.zeros(len(dcm_vector[Nsim]))
new_dvcm_vector = np.zeros(len(dcm_vector[Nsim]))
new_theta_vector = np.zeros(len(dcm_vector[Nsim]))

factor = [1,10,200,int(len(dcm_vector[Nsim]))] #Vector of the widths of the bubble


for j in range(len(factor)):  #For eache bubble with we create a new tree
    for i in range(len(dcm_vector[Nsim])):
        i_range = np.random.randint(max(0,i-factor[j]),min(len(dcm_vector[Nsim]),i+factor[j])) #Randomly sample inside the bubble
        if(i>0): #Uncomment if you don't want to keep the doubles
            while(i_range_old==i_range):
                i_range = np.random.randint(max(0,i-factor[j]),min(len(dcm_vector[Nsim]),i+factor[j])) 
        new_mass_vector[i] = nodes_plot_vector[Nsim][i_range]
        new_dcm_vector[i] = dcm_vector[Nsim][i_range]
        new_dvcm_vector[i] = dvcm_vector[Nsim][i_range]
        new_theta_vector[i] = theta_vector[Nsim][i_range]
        #if(i>0):
        #    if(i_range_old==i_range): #Check if an element is taken two times (for the moment do nothing)
        #        print("Double") 
        i_range_old = i_range


    #Now plot the new distributions

    weights= np.zeros(len(dcm_vector[Nsim]))+1./float(len(dcm_vector[Nsim]))
        
    plt.figure(3)
    fig3.add_subplot(221)
    plt.hist(new_mass_vector, bins=np.logspace(np.log10(min(new_mass_vector)),np.log10(max(new_mass_vector)),30), weights = weights,  histtype='stepfilled', facecolor='None', edgecolor=colorv[j], linewidth=2.)
    #plt.hist(new_mass_vector, bins=np.logspace(np.log10(min(new_mass_vector)),np.log10(max(new_mass_vector)),30), weights = weights,  histtype='stepfilled', facecolor='None', edgecolor="Orange", linewidth=2.)
    plt.xlabel(r'$dm$')
    plt.xscale('log')

    fig3.add_subplot(222)
    plt.hist(new_dcm_vector, bins=np.logspace(np.log10(min(new_dcm_vector)),np.log10(max(new_dcm_vector)),30), weights = weights,  histtype='stepfilled', facecolor='None', edgecolor=colorv[j],  linewidth=2.)
    #plt.hist(new_dcm_vector, bins=np.logspace(np.log10(min(new_dcm_vector)),np.log10(max(new_dcm_vector)),30), weights = weights,  histtype='stepfilled', facecolor='None', edgecolor="Orange", linewidth=2.)
    plt.xlabel(r'$d_{cm}$')
    plt.xscale('log')

    fig3.add_subplot(223)
    plt.hist(new_dvcm_vector, bins=np.logspace(np.log10(min(new_dvcm_vector)),np.log10(max(new_dvcm_vector)),30), weights = weights,  histtype='stepfilled', facecolor='None', edgecolor=colorv[j],  linewidth=2.)
    #plt.hist(new_dvcm_vector, bins=np.logspace(np.log10(min(new_dvcm_vector)),np.log10(max(new_dvcm_vector)),30), weights = weights,  histtype='stepfilled', facecolor='None', edgecolor="Orange", linewidth=2.)
    plt.xlabel(r'$dv_{cm}$')
    plt.xscale('log')

    fig3.add_subplot(224)
    plt.hist(new_theta_vector, bins=np.linspace(min(new_theta_vector),max(new_theta_vector),30), weights = weights,  histtype='stepfilled', facecolor='None', edgecolor=colorv[j], linewidth=2.)
    #plt.hist(new_theta_vector, bins=np.linspace(min(new_theta_vector),max(new_theta_vector),30), weights = weights,  histtype='stepfilled', facecolor='None', edgecolor="Orange", linewidth=2.)
    plt.xlabel(r'$\theta_{cm}$')
    #plt.xscale('log')



    appo = np.linspace(1,len(nodes_plot_vector[Nsim]),len(nodes_plot_vector[Nsim]))
    appo = appo[::-1]

    plt.figure(9)
    fig9.add_subplot(1,4,1)
    plt.plot(appo, new_mass_vector, lw=0.00001, alpha=0.5, marker=".", color=colorv[j])
    
    fig9.add_subplot(1,4,2)
    plt.plot(appo, new_dcm_vector, lw=0.00001, alpha=0.5, marker=".", color=colorv[j])
    

    fig9.add_subplot(1,4,3)
    plt.plot(appo, new_dvcm_vector, lw=0.00001, alpha=0.5, marker=".", color=colorv[j])
    
    fig9.add_subplot(1,4,4)
    plt.plot(appo, new_theta_vector, lw=0.00001, alpha=0.5, marker=".", color=colorv[j])


    fig12= plt.figure(12,figsize=(14,10))

    plt.figure(12)
    
    #plot_matrix = np.column_stack((nodes_plot_vector[i], dcm_vector[i], dvcm_vector[i]))
    appo = np.linspace(1,len(nodes_plot_vector[Nsim])-1,len(nodes_plot_vector[Nsim])-1)
    appo=appo[::-1]
    #appo=appo[1:]
    #print(plot_matrix)
   
    new_dm_vector_i = new_mass_vector[:-1]
    new_dm_vector_iplus1 = new_mass_vector[1:]

    #print(len(dm_vector_i))

    new_dcm_vector_i = new_dcm_vector[:-1]
    new_dcm_vector_iplus1 = new_dcm_vector[1:]

    new_dvcm_vector_i = new_dvcm_vector[:-1]
    new_dvcm_vector_iplus1 = new_dvcm_vector[1:]

    new_theta_vector_i = new_theta_vector[:-1]
    new_theta_vector_iplus1 = new_theta_vector[1:]


    fig12.add_subplot(2,2,1)
    #plot_matrix = np.array(sorted(plot_matrix,key=lambda x: x[0]))
    #print(plot_matrix)
    plt.scatter(new_dm_vector_i, new_dm_vector_iplus1,c=appo, norm=colors.LogNorm(), marker=".")
    plt.xlabel(r'$dm_{cm,i}$')
    plt.ylabel(r'$dm_{cm,i+1}$')
    plt.xscale('log')
    plt.yscale('log')    
    plt.xlim(0.0002,1.1)
    plt.ylim(0.0002,1.1)
    plt.clim(1,len(appo))
    if(j==0):
        plt.colorbar().ax.set_ylabel(r'$N_{splitting}$', rotation=270)

    
    fig12.add_subplot(2,2,2)
    #plot_matrix = np.array(sorted(plot_matrix,key=lambda x: x[0]))
    plt.scatter(new_dcm_vector_i, new_dcm_vector_iplus1,c=appo, norm=colors.LogNorm(), marker=".")
    plt.xlabel(r'$d_{cm,i}$')
    plt.ylabel(r'$d_{cm,i+1}$')
    plt.xscale('log')
    plt.yscale('log')    
    plt.xlim(0.0001,40)
    plt.ylim(0.0001,40)
    plt.clim(1,len(appo))
    plt.plot(np.arange(0.0001, 40), 1.0*(np.arange(0.0001, 40)**(1.)))
    if(j==0):
        plt.colorbar().ax.set_ylabel(r'$N_{splitting}$', rotation=270)

    fig12.add_subplot(2,2,3)
    #plot_matrix = np.array(sorted(plot_matrix,key=lambda x: x[0]))
    #print(plot_matrix)
    plt.scatter(new_dvcm_vector_i, new_dvcm_vector_iplus1,c=appo, norm=colors.LogNorm(), marker=".")
    plt.xlabel(r'$dv_{cm,i}$')
    plt.ylabel(r'$dv_{cm,i+1}$')
    plt.xscale('log')
    plt.yscale('log')    
    plt.xlim(0.004,20) 
    plt.ylim(0.004,20) 

    plt.clim(1,len(appo))
    plt.plot(np.arange(0.0001, 40), 1.0*(np.arange(0.0001, 40)**(1.)))

    if(j==0):
        plt.colorbar().ax.set_ylabel(r'$N_{splitting}$', rotation=270)

    fig12.add_subplot(2,2,4)
    #plot_matrix = np.array(sorted(plot_matrix,key=lambda x: x[0]))
    #print(plot_matrix)
    plt.scatter(new_theta_vector_i, new_theta_vector_iplus1,c=appo, norm=colors.LogNorm(), marker=".")
    plt.xlabel(r'$dtheta_{cm,i}$')
    plt.ylabel(r'$dtheta_{cm,i+1}$')
    #plt.xscale('log')
    #plt.yscale('log')    
    plt.xlim(-1.,1.) 
    plt.ylim(-1.,1.) 

    #plt.ylim(0.08,100) 
    #plt.clim(1,len(plot_matrix[:,1]))

    plt.clim(1,len(appo))
    if(j==0):
        plt.colorbar().ax.set_ylabel(r'$N_{splitting}$', rotation=270)

    plt.tight_layout()

    plt.savefig("new_i_iplus1_r"+str(factor[j])+".png")
    
plt.figure(3)

fig3.add_subplot(221)
plt.xlabel(r'$dm$')
plt.xscale('log')

fig3.add_subplot(222)
plt.xlabel(r'$d_{cm}$')
plt.xscale('log')

fig3.add_subplot(223)
plt.xlabel(r'$dv_{cm}$')
plt.xscale('log')

fig3.add_subplot(224)
plt.xlabel(r'$\theta_{cm}$')
#plt.xscale('log')



fig3.legend(["Original","1","10","200","All"],     # The line objects
           loc="center right",   # Position of legend
           borderaxespad=0.1,    # Small spacing around legend box
           title="Hydro Sim"  # Title for the legend
           )       
plt.legend()
#fig1.savefig('sink_distributions.png')


fig3.savefig('new_distributions.png')


plt.figure(9)
fig9.add_subplot(1,4,1)
plt.xlabel(r'$N_{splitting}$') 
plt.ylabel(r'$\Delta m_{splitting}$') 
plt.xscale('log')
plt.yscale('log')
#plt.ylim(0.0002,1.1)

fig9.add_subplot(1,4,2)
plt.xlabel(r'$N_{splitting}$') 
plt.ylabel(r'$d_{cm}$') 
plt.xscale('log')
plt.yscale('log')


fig9.add_subplot(1,4,3)
plt.xlabel(r'$N_{splitting}$') 
plt.ylabel(r'$dv_{cm}$') 
plt.xscale('log')
plt.yscale('log')

fig9.add_subplot(1,4,4)
plt.xlabel(r'$N_{splitting}$') 
plt.ylabel(r'$\theta$') 
plt.xscale('log')
#plt.yscale('log')

fig9.legend(["Original","1","10","200","All"],     # The line objects
           loc="center right",   # Position of legend
           borderaxespad=0.1,    # Small spacing around legend box
           title="Hydro Sim"  # Title for the legend
           )       
plt.legend()

plt.tight_layout()     
plt.savefig("new_parameters.png") 


#####################################################################################################################################

#####################################################################################################################################

#Now we generate the new tree. 

r_bubble = 5 # Choose a certain width for the bubble


#Create the new vector of mass ratios, c.o.m. distances and velocity c.o.m. distances
new_mass_vector = np.zeros(len(dcm_vector[Nsim]))
new_dcm_vector = np.zeros(len(dcm_vector[Nsim]))
new_dvcm_vector = np.zeros(len(dcm_vector[Nsim]))

new_dxcm_vector = np.zeros(len(dxcm_vector[Nsim]))
new_dycm_vector = np.zeros(len(dycm_vector[Nsim]))
new_dzcm_vector = np.zeros(len(dzcm_vector[Nsim]))

new_dvxcm_vector = np.zeros(len(dvxcm_vector[Nsim]))
new_dvycm_vector = np.zeros(len(dvycm_vector[Nsim]))
new_dvzcm_vector = np.zeros(len(dvzcm_vector[Nsim]))


new_theta_vector = np.zeros(len(dcm_vector[Nsim]))

for i in range(len(dcm_vector[Nsim])):
    i_range = np.random.randint(max(0,i-r_bubble),min(len(dcm_vector[Nsim]),i+r_bubble))
    if(i>0): #Uncomment if you don't want to keep the doubles
        while(i_range_old==i_range):
            i_range = np.random.randint(max(0,i-r_bubble),min(len(dcm_vector[Nsim]),i+r_bubble))

    new_mass_vector[i] = nodes_plot_vector[Nsim][i_range]
    new_dcm_vector[i] = dcm_vector[Nsim][i_range]
    new_dvcm_vector[i] = dvcm_vector[Nsim][i_range]
    new_theta_vector[i] = theta_vector[Nsim][i_range]

    new_dxcm_vector[i] = dxcm_vector[Nsim][i_range]
    new_dycm_vector[i] = dycm_vector[Nsim][i_range]
    new_dzcm_vector[i] = dzcm_vector[Nsim][i_range]

    new_dvxcm_vector[i] = dvxcm_vector[Nsim][i_range]
    new_dvycm_vector[i] = dvycm_vector[Nsim][i_range]
    new_dvzcm_vector[i] = dvzcm_vector[Nsim][i_range]
    #if(i>0):
    #    if(i_range_old==i_range):
    #        print("Double") #Check if the point sampled is double
    i_range_old = i_range

#We now have created new vectors of distances in the mass, position of velocity space. In order to create a new stellar system we need to split the distances in separate stars


#Create the new masses, positions and velocities from the tree just sampled

new_splitted_m = np.zeros((len(dcm_vector[Nsim])+1,2))
new_splitted_x = np.zeros((len(dcm_vector[Nsim])+1,2))
new_splitted_y = np.zeros((len(dcm_vector[Nsim])+1,2))
new_splitted_z = np.zeros((len(dcm_vector[Nsim])+1,2))

new_splitted_vx = np.zeros((2*len(dcm_vector[Nsim])+1,2))
new_splitted_vy = np.zeros((2*len(dcm_vector[Nsim])+1,2))
new_splitted_vz = np.zeros((2*len(dcm_vector[Nsim])+1,2))



new_splitted_labels = np.zeros((len(dcm_vector[Nsim])+1,2))

#Reverse the vectors (the leaves now are at the end)

new_mass_vector=new_mass_vector[::-1]
new_dcm_vector=new_dcm_vector[::-1]
new_dxcm_vector=new_dxcm_vector[::-1]
new_dycm_vector=new_dycm_vector[::-1]
new_dzcm_vector=new_dzcm_vector[::-1]
new_dvxcm_vector=new_dvxcm_vector[::-1]
new_dvycm_vector=new_dvycm_vector[::-1]
new_dvzcm_vector=new_dvzcm_vector[::-1]


pick_up_vector = [] #In this vector we store the masses that can be splitted again
parent_vector= np.zeros((len(dcm_vector[Nsim])+1,2)) #In this vector we will put the branches that can be splitted again (it an appo matrix)

#These vectors will contain the final leaves of the tree
x_leaves=[]
y_leaves=[]
z_leaves=[]
vx_leaves=[]
vy_leaves=[]
vz_leaves=[]


label_counter = 1
i=0
while(len(pick_up_vector)<len(m_gand0) and i<len(new_mass_vector)):
    if(i==0): #First splitting
        #take the i-th splitting and separate the masses

        new_splitted_m[i,0]=(new_mass_vector[i]+1.)/2.*scales_m[Nsim] #Multiply for the mass scale to return to physical units
        new_splitted_m[i,1]=(1.-new_mass_vector[i])/2.*scales_m[Nsim]

        parent_vector[i,0]=new_splitted_m[i,0] 
        parent_vector[i,1]=new_splitted_m[i,1]

        new_splitted_labels[i,0] = label_counter
        label_counter+=1
        new_splitted_labels[i,1] = label_counter

        pick_up_vector.append(new_splitted_m[i,0]) #The new splitted masses are put in pick_up_vector so they can be then splitted again
        pick_up_vector.append(new_splitted_m[i,1]) #The new splitted masses are put in pick_up_vector so they can be then splitted again

        #print(pick_up_vector)

        #Now split the positions and velocities
        
        new_splitted_x[i,1]= (0. - new_dxcm_vector[i]*(new_splitted_m[i,0])/(new_splitted_m[i,0]+new_splitted_m[i,1]))#*scales_r[Nsim]
        new_splitted_x[i,0]= (0. + new_dxcm_vector[i]*(new_splitted_m[i,1])/(new_splitted_m[i,0]+new_splitted_m[i,1]))#*scales_r[Nsim]

        new_splitted_y[i,1]= (0. - new_dycm_vector[i]*(new_splitted_m[i,0])/(new_splitted_m[i,0]+new_splitted_m[i,1]))#*scales_r[Nsim]
        new_splitted_y[i,0]= (0. + new_dycm_vector[i]*(new_splitted_m[i,1])/(new_splitted_m[i,0]+new_splitted_m[i,1]))#*scales_r[Nsim]

        new_splitted_z[i,1]= (0. - new_dzcm_vector[i]*(new_splitted_m[i,0])/(new_splitted_m[i,0]+new_splitted_m[i,1]))#*scales_r[Nsim]
        new_splitted_z[i,0]= (0. + new_dzcm_vector[i]*(new_splitted_m[i,1])/(new_splitted_m[i,0]+new_splitted_m[i,1]))#*scales_r[Nsim]

        new_splitted_vx[i,1]= (0. - new_dvxcm_vector[i]*(new_splitted_m[i,0])/(new_splitted_m[i,0]+new_splitted_m[i,1]))#*scales_v[Nsim]
        new_splitted_vx[i,0]= (0. + new_dvxcm_vector[i]*(new_splitted_m[i,1])/(new_splitted_m[i,0]+new_splitted_m[i,1]))#*scales_v[Nsim]

        new_splitted_vy[i,1]= (0. - new_dvycm_vector[i]*(new_splitted_m[i,0])/(new_splitted_m[i,0]+new_splitted_m[i,1]))#*scales_v[Nsim]
        new_splitted_vy[i,0]= (0. + new_dvycm_vector[i]*(new_splitted_m[i,1])/(new_splitted_m[i,0]+new_splitted_m[i,1]))#*scales_v[Nsim]

        new_splitted_vz[i,1]= (0. - new_dvzcm_vector[i]*(new_splitted_m[i,0])/(new_splitted_m[i,0]+new_splitted_m[i,1]))#*scales_v[Nsim]
        new_splitted_vz[i,0]= (0. + new_dvzcm_vector[i]*(new_splitted_m[i,1])/(new_splitted_m[i,0]+new_splitted_m[i,1]))#*scales_v[Nsim]

        #Add the new splitting to the leaves vectors
        x_leaves.append(new_splitted_x[i,0])
        y_leaves.append(new_splitted_y[i,0])
        z_leaves.append(new_splitted_z[i,0])
        x_leaves.append(new_splitted_x[i,1])
        y_leaves.append(new_splitted_y[i,1])
        z_leaves.append(new_splitted_z[i,1])
        
        vx_leaves.append(new_splitted_vx[i,0])
        vy_leaves.append(new_splitted_vy[i,0])
        vz_leaves.append(new_splitted_vz[i,0])
        vx_leaves.append(new_splitted_vx[i,1])
        vy_leaves.append(new_splitted_vy[i,1])
        vz_leaves.append(new_splitted_vz[i,1])

    else:
        #We have to chose under which criterion to select the mass to split
        #We decide to take from the pick_up_vector three random masses and from these masses we select the most massive one
        
        Num = 3
        num_to_split=np.random.choice(pick_up_vector, min(len(pick_up_vector),Num))
        m_to_split=max(num_to_split)
        
        ind_to_split=np.argwhere(parent_vector==m_to_split) #Find the indexes of the masses to split in the parent_vector

        mass_of_branch = new_splitted_m[ind_to_split[0][0],ind_to_split[0][1]]
         
        new_splitted_m[i,0]=(new_mass_vector[i]+1.)*mass_of_branch/2.
        new_splitted_m[i,1]=(1.-new_mass_vector[i])*mass_of_branch/2.
        
        #Separate the new positions and velocities from that of the splitted mass
        """
        x_of_branch =
        y_of_branch =
        z_of_branch =
        vx_of_branch =
        vy_of_branch =
        vz_of_branch =        
        """

        ind_0 = np.random.choice([0,1])
        ind_1 = 1- ind_0 

        new_splitted_x[i,ind_1]= (new_splitted_x[ind_to_split[0][0],ind_to_split[0][1]] - new_dxcm_vector[i]*(new_splitted_m[i,ind_0])/(new_splitted_m[i,0]+new_splitted_m[i,1]))
        new_splitted_x[i,ind_0]= (new_splitted_x[ind_to_split[0][0],ind_to_split[0][1]] + new_dxcm_vector[i]*(new_splitted_m[i,ind_1])/(new_splitted_m[i,0]+new_splitted_m[i,1]))

        ind_0 = np.random.choice([0,1])
        ind_1 = 1- ind_0 

        new_splitted_y[i,ind_1]= (new_splitted_y[ind_to_split[0][0],ind_to_split[0][1]] - new_dycm_vector[i]*(new_splitted_m[i,ind_0])/(new_splitted_m[i,0]+new_splitted_m[i,1]))
        new_splitted_y[i,ind_0]= (new_splitted_y[ind_to_split[0][0],ind_to_split[0][1]] + new_dycm_vector[i]*(new_splitted_m[i,ind_1])/(new_splitted_m[i,0]+new_splitted_m[i,1]))

        ind_0 = np.random.choice([0,1])
        ind_1 = 1- ind_0 

        new_splitted_z[i,ind_1]= (new_splitted_z[ind_to_split[0][0],ind_to_split[0][1]] - new_dzcm_vector[i]*(new_splitted_m[i,ind_0])/(new_splitted_m[i,0]+new_splitted_m[i,1]))
        new_splitted_z[i,ind_0]= (new_splitted_z[ind_to_split[0][0],ind_to_split[0][1]] + new_dzcm_vector[i]*(new_splitted_m[i,ind_1])/(new_splitted_m[i,0]+new_splitted_m[i,1]))

        ind_0 = np.random.choice([0,1])
        ind_1 = 1- ind_0  

        new_splitted_vx[i,ind_1]= (new_splitted_vx[ind_to_split[0][0],ind_to_split[0][1]] - new_dvxcm_vector[i]*(new_splitted_m[i,ind_0])/(new_splitted_m[i,0]+new_splitted_m[i,1]))
        new_splitted_vx[i,ind_0]= (new_splitted_vx[ind_to_split[0][0],ind_to_split[0][1]] + new_dvxcm_vector[i]*(new_splitted_m[i,ind_1])/(new_splitted_m[i,0]+new_splitted_m[i,1]))

        ind_0 = np.random.choice([0,1])
        ind_1 = 1- ind_0 

        new_splitted_vy[i,ind_1]= (new_splitted_vy[ind_to_split[0][0],ind_to_split[0][1]] - new_dvycm_vector[i]*(new_splitted_m[i,ind_0])/(new_splitted_m[i,0]+new_splitted_m[i,1]))
        new_splitted_vy[i,ind_0]= (new_splitted_vy[ind_to_split[0][0],ind_to_split[0][1]] + new_dvycm_vector[i]*(new_splitted_m[i,ind_1])/(new_splitted_m[i,0]+new_splitted_m[i,1]))

        ind_0 = np.random.choice([0,1])
        ind_1 = 1- ind_0 

        new_splitted_vz[i,ind_1]= (new_splitted_vz[ind_to_split[0][0],ind_to_split[0][1]] - new_dvzcm_vector[i]*(new_splitted_m[i,ind_0])/(new_splitted_m[i,0]+new_splitted_m[i,1]))
        new_splitted_vz[i,ind_0]= (new_splitted_vz[ind_to_split[0][0],ind_to_split[0][1]] + new_dvzcm_vector[i]*(new_splitted_m[i,ind_1])/(new_splitted_m[i,0]+new_splitted_m[i,1]))

        #Add the new splitting to the leaves vectors 
        x_leaves.append(new_splitted_x[i,0])
        y_leaves.append(new_splitted_y[i,0])
        z_leaves.append(new_splitted_z[i,0])
        x_leaves.append(new_splitted_x[i,1])
        y_leaves.append(new_splitted_y[i,1])
        z_leaves.append(new_splitted_z[i,1])
        
        vx_leaves.append(new_splitted_vx[i,0])
        vy_leaves.append(new_splitted_vy[i,0])
        vz_leaves.append(new_splitted_vz[i,0])
        vx_leaves.append(new_splitted_vx[i,1])
        vy_leaves.append(new_splitted_vy[i,1])
        vz_leaves.append(new_splitted_vz[i,1])

        #The splitted particle is no more a leave, it's a branch
        x_leaves.remove(new_splitted_x[ind_to_split[0][0],ind_to_split[0][1]])
        y_leaves.remove(new_splitted_y[ind_to_split[0][0],ind_to_split[0][1]])
        z_leaves.remove(new_splitted_z[ind_to_split[0][0],ind_to_split[0][1]])

        vx_leaves.remove(new_splitted_vx[ind_to_split[0][0],ind_to_split[0][1]])
        vy_leaves.remove(new_splitted_vy[ind_to_split[0][0],ind_to_split[0][1]])
        vz_leaves.remove(new_splitted_vz[ind_to_split[0][0],ind_to_split[0][1]])

        parent_vector[i,0]=new_splitted_m[i,0]
        parent_vector[i,1]=new_splitted_m[i,1]

        pick_up_vector.append(new_splitted_m[i,0]) #The new splitted masses are put in pick_up_vector so they can be then splitted again
        pick_up_vector.append(new_splitted_m[i,1]) #The new splitted masses are put in pick_up_vector so they can be then splitted again
        pick_up_vector.remove(m_to_split) #The splitted mass cannot be splitted again


    i+=1

x_leaves = np.array(x_leaves)
y_leaves = np.array(y_leaves)
z_leaves = np.array(z_leaves)

vx_leaves = np.array(vx_leaves)
vy_leaves = np.array(vy_leaves)
vz_leaves = np.array(vz_leaves)


x_leaves = x_leaves*scales_r[Nsim]
y_leaves = y_leaves*scales_r[Nsim]
z_leaves = z_leaves*scales_r[Nsim]

vx_leaves = vx_leaves*scales_v[Nsim]
vy_leaves = vy_leaves*scales_v[Nsim]
vz_leaves = vz_leaves*scales_v[Nsim]


#Plot the new created distributions
new_splitted_m.flatten()

plt.figure(figsize=(8,8))
#print(new_splitted_m.flatten())
print(len(pick_up_vector),len(m_gand0), np.sum(pick_up_vector))
plt.hist(m_gand0, bins=np.logspace(np.log10(min(min(pick_up_vector),min(m_gand0))),np.log10(max(max(pick_up_vector),max(m_gand0))),40),histtype='stepfilled',facecolor='None',edgecolor="Blue", label="Original")
plt.hist(pick_up_vector,bins=np.logspace(np.log10(min(min(pick_up_vector),min(m_gand0))),np.log10(max(max(pick_up_vector),max(m_gand0))),40),histtype='stepfilled',facecolor='None',edgecolor="Orange", label="New")
print(new_mass_vector)
plt.xlabel(r"$m [M_{\odot}]$")
plt.xscale("log")
plt.yscale("log")
plt.legend()
plt.savefig("new_mass_splitted_distribution.png")
plt.close()



fig4=plt.figure(figsize=(16,10))
fig4.add_subplot(231)
plt.hist(x_gand0, bins=np.linspace(min(x_gand0), max(x_gand0),40), histtype='stepfilled',facecolor='None',edgecolor="Blue", label="Original")
plt.hist(x_leaves, bins=np.linspace(min(x_gand0), max(x_gand0),40), histtype='stepfilled',facecolor='None',edgecolor="Orange", label="New")
plt.xlabel(r"$x [pc]$")
plt.legend()

fig4.add_subplot(232)
plt.hist(y_gand0, bins=np.linspace(min(y_gand0), max(y_gand0),40), histtype='stepfilled',facecolor='None',edgecolor="Blue", label="Original")
plt.hist(y_leaves, bins=np.linspace(min(y_gand0), max(y_gand0),40), histtype='stepfilled',facecolor='None',edgecolor="Orange", label="New")
plt.xlabel(r"$y [pc]$")
plt.legend()

fig4.add_subplot(233)
plt.hist(z_gand0, bins=np.linspace(min(z_gand0), max(z_gand0),40), histtype='stepfilled',facecolor='None',edgecolor="Blue", label="Original")
plt.hist(z_leaves, bins=np.linspace(min(z_gand0), max(z_gand0),40),  histtype='stepfilled',facecolor='None',edgecolor="Orange", label="New")
plt.xlabel(r"$z [pc]$")
plt.legend()

fig4.add_subplot(234)
plt.hist(vx_gand0, bins=np.linspace(min(vx_gand0), max(vx_gand0),40), histtype='stepfilled',facecolor='None',edgecolor="Blue", label="Original")
plt.hist(vx_leaves, bins=np.linspace(min(vx_gand0), max(vx_gand0),40), histtype='stepfilled',facecolor='None',edgecolor="Orange", label="New")
plt.xlabel(r"$v_x [km/s]$")
plt.legend()

fig4.add_subplot(235)
plt.hist(vy_gand0, bins=np.linspace(min(vy_gand0), max(vy_gand0),40), histtype='stepfilled',facecolor='None',edgecolor="Blue", label="Original")
plt.hist(vy_leaves, bins=np.linspace(min(vy_gand0), max(vy_gand0),40), histtype='stepfilled',facecolor='None',edgecolor="Orange", label="New")
plt.xlabel(r"$v_y [km/s]$")
plt.legend()

fig4.add_subplot(236)
plt.hist(vz_gand0, bins=np.linspace(min(vz_gand0), max(vz_gand0),40), histtype='stepfilled',facecolor='None',edgecolor="Blue", label="Original")
plt.hist(vz_leaves,bins=np.linspace(min(vz_gand0), max(vz_gand0),40), histtype='stepfilled',facecolor='None',edgecolor="Orange", label="New")
plt.xlabel(r"$v_z [km/s]$")
plt.legend()

#plt.xscale("log")
#plt.yscale("log")
plt.savefig("new_pos_vel_splitted_distribution.png")
plt.close()



fig4=plt.figure(figsize=(16,10))
fig4.add_subplot(231)
plt.scatter(x_gand0, y_gand0, color="Blue",s=1, label="Original")
plt.scatter(x_leaves, y_leaves, color="Orange",s=1, label="New")
plt.xlabel(r"$x [pc]$")
plt.ylabel(r"$y [pc]$")
plt.legend()


fig4.add_subplot(232)
plt.scatter(y_gand0, z_gand0, color="Blue",s=1, label="Original")
plt.scatter(y_leaves, z_leaves, color="Orange",s=1, label="New")
plt.xlabel(r"$y [pc]$")
plt.ylabel(r"$z [pc]$")
plt.legend()


fig4.add_subplot(233)
plt.scatter(z_gand0, x_gand0, color="Blue",s=1, label="Original")
plt.scatter(z_leaves, x_leaves, color="Orange",s=1, label="New")
plt.xlabel(r"$z [pc]$")
plt.ylabel(r"$x [pc]$")
plt.legend()


fig4.add_subplot(234)
plt.scatter(vx_gand0, vy_gand0, color="Blue",s=1, label="Original")
plt.scatter(vx_leaves, vy_leaves, color="Orange",s=1, label="New")
plt.xlabel(r"$v_x [km/s]$")
plt.ylabel(r"$v_y [km/s]$")
plt.legend()


fig4.add_subplot(235)
plt.scatter(vy_gand0, vz_gand0, color="Blue",s=1, label="Original")
plt.scatter(vy_leaves, vz_leaves, color="Orange",s=1, label="New")
plt.xlabel(r"$v_y [km/s]$")
plt.ylabel(r"$v_z [km/s]$")
plt.legend()

fig4.add_subplot(236)
plt.scatter(vz_gand0, vx_gand0, color="Blue",s=1, label="Original")
plt.scatter(vz_leaves, vx_leaves, color="Orange",s=1, label="New")
plt.xlabel(r"$v_z [km/s]$")
plt.ylabel(r"$v_x [km/s]$")
plt.legend()



#plt.xscale("log")
#plt.yscale("log")
plt.savefig("new_pos_vel_splitted_distribution_scatter.png")
plt.close()
"""
for i in range(X.shape[0]-1):
    for j in range(2):
        if(clustering.children_[i][j]<X.shape[0]):
            leaves_mass[i]+=m[clustering.children_[i][j]]
        else:
            leaves_mass[i]+=leaves_mass[clustering.children_[i][j]-X.shape[0]]

for i in range(X.shape[0]-1):
    for j in range(2):
        if(clustering.children_[i][j]<X.shape[0]):
            nodes_ratios[i][j]+=m[clustering.children_[i][j]]/leaves_mass[i]
            mcm[i][j]+=m[clustering.children_[i][j]]
            #nodes_ratios1[i][j]+=m[clustering.children_[i][j]]/(leaves_mass[i]-m[clustering.children_[i][j]])
        else:
            nodes_ratios[i][j]+=leaves_mass[clustering.children_[i][j]-X.shape[0]]/leaves_mass[i]
            mcm[i][j]+=leaves_mass[clustering.children_[i][j]-X.shape[0]]
            #nodes_ratios1[i][j]+=leaves_mass[clustering.children_[i][j]-X.shape[0]]/(leaves_mass[i]-leaves_mass[clustering.children_[i][j]-X.shape[0]])
    #print(nodes_ratios[i])

nodes_plot = []
"""



